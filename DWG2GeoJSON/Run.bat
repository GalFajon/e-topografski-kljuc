SET EXE=%~dp0/LibreDWG/dwgread.exe
SET RAW=%~dp0/RAW
SET OUT=%~dp0/OUT

for %%f in (%RAW%/*.dwg) do %EXE% %RAW%/%%~nf.dwg -O GeoJSON -o %OUT%/%%~nf.geojson

pause