/**
 * @swagger
 * tags:
 *   name: Users
 *   description: Add, update or delete user accounts.
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     User:
 *       type: object
 *       properties:
 *          UID_DTA:
 *           type: uuid
 *           description: A uuid identifying the user.
 *           example: b2dbc2de-2ad9-47bb-ba86-dc6c0c8fbe28
 *          DTA_EML:
 *           type: string
 *           description: The email address of the account.
 *           example: example.address@gmail.com
 *          DTA_PSW:
 *           type: string
 *           description: An encrypted string containing the users password.
 *           example: password123
 *          DTA_ACT:
 *           type: boolean
 *           description: A boolean signifying whether or not the account is active.
 *           example: true/false
 */

let express = require('express');
const router = express.Router();
const dataBaseInterface = require('../DataBase/Connector/DataBase.js');

const authenticationMiddleware = require('../authentication/middleware.js');

const SqlString = require('sqlstring');

/**
* @swagger
* /users:
*  post:
*   tags: [Users]
*   description: Adds new (not activated) user to the database.
*   parameters:
*       - in: DTA_EML
*         name: DTA_EML
*         description: The email address of the account.
*         example: example.mail@gmail.com
*       - in: DTA_PSW
*         name: DTA_PSW
*         description: The unencrypted password of the account.
*         example: password123
*   responses:
*       "201":
*           description: "User added."
*       "400":
*           description: "Error. Incorrect input."
*       "500":
*           description: "Error. Database server error."
*/
router.post('/', async function (req, res) {
    if (req.body) {
        let valid = true;

        if (! (req.body.DTA_EML && typeof req.body.DTA_EML === 'string')) valid = false;
        if (! (req.body.DTA_PSW && typeof req.body.DTA_PSW === 'string')) valid = false;
        req.body.UID_DTA = SqlString.raw(`uuid_generate_v4()`);
        req.body.DTA_ACT = false;

        if (!valid) { res.status(400).send('Error. Incorrect input.'); return; }

        await dataBaseInterface.SQL_FILE_US('./DataBase/Queries/users/add_new.sql', [req.body.UID_DTA, req.body.DTA_EML, req.body.DTA_PSW, req.body.DTA_ACT], (err, result) => {
            if (err) { res.status(500).send('Error. Database server error.'); return; }
            else res.status(201).send("User added.");
        });
    }
    else { res.status(400).send('Error. Incorrect input.'); return; }
})

/**
* @swagger
* /users:
*  get:
*   tags: [Users]
*   description: Gets all users. Requires the user to be logged in.
*   responses:
*       "200":
*           description: "User was found."
*           content:
*               application/json:
*                   schema:
*                       type: object
*                       $ref: '#/components/schemas/User'
*       "401":
*           description: "You must have an active account to perform this action."
*       "500":
*           description: "Error. Database server error."
*/
router.get('/', authenticationMiddleware.verify, async function (req, res) {
    if (req.user && req.user.DTA_ACT == true) {
        await dataBaseInterface.SQL_FILE_CS('./DataBase/Queries/users/get_all.sql', undefined, (err, result) => {
            if (err) res.status(500).send('Error. Database server error.');
            else res.status(200).json(result.rows);
        });
    }
    else res.status(401).send("You must have an active account to perform this action.");
})

/**
* @swagger
* /users/{uuid}:
*  put:
*   tags: [Users]
*   description: Updates a particular user with data. Requires the user to be logged into an active account or editing their own account.
*   parameters:
*       - in: uuid
*         name: uuid
*         description: The uuid of the user.
*         example: b2dbc2de-2ad9-47bb-ba86-dc6c0c8fbe28
*       - in: DTA_EML
*         name: DTA_EML
*         description: The email address of the account.
*         example: example.mail@gmail.com
*       - in: DTA_PSW
*         name: DTA_PSW
*         description: The unencrypted password of the account.
*         example: password123
*       - in: DTA_ACT
*         name: DTA_ACT
*         description: A boolean describing whether or not the account is activated.
*         example: true/false
*   responses:
*       "201":
*           description: "User updated."
*       "400":
*           description: "Error. Incorrect input."
*       "401":
*           description: "Error. You must be logged in to perform this action."
*       "500":
*           description: "Error. Database server error."
*/
router.put('/:uuid', authenticationMiddleware.verify, async function (req, res) {  
    if (req.user) {
        if (req.body) {
            let valid = true;
            if (Object.keys(req.body).includes('UID_DTA')) valid = false;
            if (! (!req.body.DTA_EML || (typeof req.body.DTA_EML === 'string' && (req.user.UID_DTA == req.params.uuid || req.user.DTA_ACT == true)))) valid = false;
            if (! (!req.body.DTA_PSW || (typeof req.body.DTA_PSW === 'string' && (req.user.UID_DTA == req.params.uuid || req.user.DTA_ACT == true)))) valid = false;
            else if(req.body.DTA_PSW) { req.body.DTA_PSW = SqlString.raw(`crypt('${req.body.DTA_PSW}', "DTA_PSW")`)}
            if (! (!req.body.DTA_ACT || ((req.body.DTA_ACT == 'true' || req.body.DTA_ACT == 'false') && req.user.DTA_ACT == true))) valid = false;

            if (!valid) {res.status(400).send('Error. Incorrect input or incorrect account permissions.'); return;}
            await dataBaseInterface.SQL_FILE_US('./DataBase/Queries/users/update.sql', [req.body, req.params.uuid], (err, result) => {
                if (err) {res.status(500).send('Error. Database server error.'); return; }
                else res.status(201).send("User updated.");
            });
        }
        else {
            res.status(400).send('Error. Incorrect input.');
            return;
        }
    }
    else {
        res.status(401).send("You must be logged in to perform this action.");
    }
})

/**
* @swagger
* /users/{uuid}:
*  delete:
*   tags: [Users]
*   description: Deletes a particular user. Requires the user to be logged into an active account or editing their own account.
*   parameters:
*       - in: uuid
*         name: uuid
*         description: The uuid of the user.
*         example: b2dbc2de-2ad9-47bb-ba86-dc6c0c8fbe28
*   responses:
*       "200":
*           description: "User deleted."
*       "400":
*           description: "Error. Incorrect input."
*       "401":
*           description: "You must be logged in to perform this action."
*       "500":
*           description: "Database server error."
*/
router.delete('/:uuid', authenticationMiddleware.verify,async function (req, res) {
    if (req.user) {
        if (! /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/.test(req.params.uuid)) {
            res.status(400).send("Error. Incorrect input."); return;
        }
        else {
            if (req.params.uuid == req.user.UID_DTA || req.user.DTA_ACT == true) {
                await dataBaseInterface.SQL_FILE_CS('./DataBase/Queries/users/delete.sql', req.params.uuid, (err, result) => {
                    if (err) res.status(500).send("Database server error.");
                    else res.status(200).send("User deleted.");
                });
            }
            else res.status(401).send("You must be logged in to perform this action.");
        }
    }
    else res.status(401).send("You must be logged in to perform this action.");
})

/**
* @swagger
* /users/login/:
*  post:
*   tags: [Users]
*   description: Creates a session cookie, indicating the user is logged in.
*   parameters:
*       - in: DTA_EML
*         name: DTA_EML
*         description: The DTA_EML (email address) of the user.
*         example: example.mail@gmail.com
*       - in: DTA_PSW
*         name: DTA_PSW
*         description: The DTA_PSW (password) of the user.
*         example: password123
*   responses:
*       "200":
*           description: "Signed in."
*       "400":
*           description: "User credentials don't match"
*/
router.post('/login/', authenticationMiddleware.verify, async function (req, res) {
    await dataBaseInterface.SQL_FILE_US('./DataBase/Queries/users/get_by_email_and_psw.sql', [req.body.DTA_EML, req.body.DTA_PSW], (err,result) => {
        if (result.rows[0]) {
            req.session.userid = result.rows[0].UID_DTA;
            res.status(200).send("Signed in.");
        }
        else res.status(400).send("User credentials don't match");
    });
})

/**
* @swagger
* /users/logout/:
*  get:
*   tags: [Users]
*   description: Clears current user session.
*   responses:
*       "200":
*           description: "Signed out."
*/
router.get('/logout/', async function (req, res) {
    req.session.destroy();
    res.status(200);
    res.send("Signed out.");
})

/**
* @swagger
* /users/currentuser/:
*  get:
*   tags: [Users]
*   description: Returns data of the user that is currently logged in.
*   responses:
*       "200":
*            description: "User was found."
*            content:
*               application/json:
*                   schema:
*                       type: object
*                       properties:
*                           UID_DTA:
*                               type: uuid
*                               description: A uuid identifying the user.
*                               example: b2dbc2de-2ad9-47bb-ba86-dc6c0c8fbe28
*                           DTA_EML:
*                               type: string
*                               description: The email address of the account.
*                               example: example.address@gmail.com
*                           DTA_ACT:
*                               type: boolean
*                               description: A boolean signifying whether or not the account is active.
*                               example: true/false
*/
router.get('/currentuser/', authenticationMiddleware.verify, async function(req,res) {
    if(req.user) {
        res.status(200);
        res.json(req.user);
    }
    else {
        res.json({});
    }

})

/**
* @swagger
* /users/{uuid}/:
*  get:
*   tags: [Users]
*   description: Gets a particular user. Requires the user to be logged in.
*   parameters:
*       - in: uuid
*         name: uuid
*         description: The uuid of the user.
*         example: b2dbc2de-2ad9-47bb-ba86-dc6c0c8fbe28
*   responses:
*       "200":
*           description: "User was found."
*           content:
*               application/json:
*                   schema:
*                       type: object
*                       $ref: '#/components/schemas/User'
*       "401":
*           description: "You must have an active account to perform this action."
*       "500":
*           description: "Error. Database server error."
*/
router.get('/:uuid', authenticationMiddleware.verify, async function(req,res) {
    if (req.user && req.user.DTA_ACT == true) {
        await dataBaseInterface.SQL_FILE_CS('./DataBase/Queries/users/get_by_uuid.sql', req.params.uuid, (err, result) => {
            if (err) res.status(500).send('Error. Database server error.');
            else res.status(200).json(result.rows[0]);
        });
    }
    else res.status(401).send("You must have an active account to perform this action.");
})

module.exports = router;