/**
 * @swagger
 * tags:
 *   name: Symbols
 *   description: Add, update or delete symbols.
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     Symbol:
 *       type: object
 *       properties:
 *         UID_DTA:
 *           type: uuid
 *           description: A uuid belonging to the symbol.
 *           example: b2dbc2de-2ad9-47bb-ba86-dc6c0c8fbe28
 *         UID_TYP:
 *           type: uuid
 *           description: A uuid that marks the symbol type (line, point etc.).
 *           example: b2dbc2de-2ad9-47bb-ba86-dc6c0c8fbe28
 *         UID_CLR:
 *           type: uuid
 *           description: A uuid that marks the symbol color (red, black etc.).
 *           example: b2dbc2de-2ad9-47bb-ba86-dc6c0c8fbe28
 *         UID_CAT:
 *           type: uuid
 *           description: A uuid that marks the symbol category. (border, building etc.)
 *           example: b2dbc2de-2ad9-47bb-ba86-dc6c0c8fbe28
 *         DTA_TXT:
 *           type: string
 *           description: The name of the symbol.
 *           example: "Sewer shaft."
 *         DTA_DESC: 
 *           type: string
 *           description: A text description of the symbol.
 *           example: "A symbol representing a sewer shaft."
 *         DTA_UID:
 *           type: int/string
 *           description: A six number ID for the symbol, once it's added to the register.
 *           example: 123456
 *         DTA_ACT:
 *           type: boolean
 *           description: A boolean that marks whether or not the symbol has been accepted.
 *           example: true/false
 *         DTA_USR:
 *           type: string/number
 *           description: The contact of the user that added the symbol.
 *           example: example.mail@gmail.com
 *         DTA_SVG:
 *           type: file
 *           description: A ".svg" file containing a vector image of the symbol. When fetched from the server it becomes a string, containing the path to the file.
 *           example: ./svg/123456.svg
 *         DTA_SIZE_SVG:
 *           type: file
 *           description: A ".svg" file containing a vector image of the symbol with added dimensions. When fetched from the server it becomes a string, containing the path to the file.
 *           example: ./size_svg/123456.svg
 *         DTA_DWG:
 *           type: file
 *           description: A ".dwg" file containing an autocad drawing file of the symbol. When fetched from the server it becomes a string, containing the path to the file.
 *           example: ./dwg/123456.dwg
 *         DTA_SHP:
 *           type: file
 *           description: A ".shp" file containing an autocad shape file of the symbol. When fetched from the server it becomes a string, containing the path to the file.
 *           example: ./shp/123456.shp
 *         DTA_LIN:
 *           type: file
 *           description: A ".lin" file containing an autocad line file of the symbol. When fetched from the server it becomes a string, containing the path to the file.
 *           example: ./lin/123456.lin
 *         DTA_ATT:
 *           type: file
 *           description: An image that can be attached to the symbol suggestion. (.jpg, .png, .svg)
 *           example: ./attachments/123456.jpg
 */

let express = require('express');
const router = express.Router();

const dataBaseInterface = require('../DataBase/Connector/DataBase.js');

const authenticationMiddleware = require('../authentication/middleware.js');
const SqlString = require('sqlstring');
const uuid = require('uuid');
const path = require('path');

/**
* @swagger
* /symbols:
*  get:
*   tags: [Symbols]
*   description: Gets all symbols from the database.
*   responses:
*       "200":
*           description: "Data was found."
*           content:
*               application/json:
*                   schema:
*                       type: object
*                       $ref: '#/components/schemas/Symbol'
*       "500":
*           description: "Error. Database server error."
*/

router.get('/', async function (req, res) {
    await dataBaseInterface.SQL_FILE_CS('./DataBase/Queries/symbols/get_all.sql', undefined, (err, result) => {
        if (err) res.status(500).send('Error. Database server error.');
        else res.status(200).json(result.rows);
    });
})

/**
* @swagger
* /symbols/cut_contours/:
*  get:
*   tags: [Symbols]
*   description: Gets symbols that cut contours.
*   responses:
*       "200":
*           description: "Symbols retrieved."
*       "400":
*           description: "Error. Incorrect input."
*       "500":
*           description: "Database server error."
*/
router.get('/cut_contours/', async function(req,res) {
    await dataBaseInterface.SQL_FILE_CS('./DataBase/Queries/symbols/get_cuts_contours.sql', req.params.type, (err, result) => {
        if (err) res.status(500).send("Error. Failed to get data.");
        else res.status(200).json(result.rows);
    });
})

/**
* @swagger
* /symbols/doesnt_cut_contours/:
*  get:
*   tags: [Symbols]
*   description: Gets symbols that don't cut contours.
*   responses:
*       "200":
*           description: "Symbols retrieved."
*       "400":
*           description: "Error. Incorrect input."
*       "500":
*           description: "Database server error."
*/
router.get('/doesnt_cut_contours/', async function(req,res) {
    await dataBaseInterface.SQL_FILE_CS('./DataBase/Queries/symbols/get_doesnt_cuts_contours.sql', req.params.type, (err, result) => {
        if (err) res.status(500).send("Error. Failed to get data.");
        else res.status(200).json(result.rows);
    });
})


/**
* @swagger
* /symbols/notactive:
*  get:
*   tags: [Symbols]
*   description: Gets all non-active symbols from the database.
*   responses:
*       "200":
*           description: "Data was found."
*           content:
*               application/json:
*                   schema:
*                       type: object
*                       $ref: '#/components/schemas/Symbol'
*       "500":
*           description: "Error. Database server error."
*/

router.get('/notactive', async function (req, res) {
    await dataBaseInterface.SQL_FILE_CS('./DataBase/Queries/symbols/get_not_active.sql', undefined, (err, result) => {
        if (err) res.status(500).send('Error. Database server error.');
        else res.status(200).json(result.rows);
    });
});

/**
* @swagger
* /symbols/active:
*  get:
*   tags: [Symbols]
*   description: Gets all active symbols from the database.
*   responses:
*       "200":
*           description: "Data was found."
*           content:
*               application/json:
*                   schema:
*                       type: object
*                       $ref: '#/components/schemas/Symbol'
*       "500":
*           description: "Error. Database server error."
*/

router.get('/active', async function (req, res) {
    await dataBaseInterface.SQL_FILE_CS('./DataBase/Queries/symbols/get_active.sql', undefined, (err, result) => {
        if (err) res.status(500).send('Error. Database server error.');
        else res.status(200).json(result.rows);
    });
});

/**
* @swagger
* /symbols:
*  post:
*   tags: [Symbols]
*   description: Adds new (not activated) symbol to the database.
*   consumes:
*       - "multipart/form-data"
*   parameters:
*       - in: UID_TYP
*         name: UID_TYP
*         description: The uuid of the symbol type.
*         example: b2dbc2de-2ad9-47bb-ba86-dc6c0c8fbe28
*         required: false
*       - in: UID_CLR
*         name: UID_CLR
*         description: The uuid of the symbol color.
*         example: b2dbc2de-2ad9-47bb-ba86-dc6c0c8fbe28
*         required: false
*       - in: UID_CAT
*         name: UID_CAT
*         description: The uuid of the symbol category.
*         example: b2dbc2de-2ad9-47bb-ba86-dc6c0c8fbe28
*         required: false
*       - in: DTA_TXT
*         name: DTA_TXT
*         description: The name of the symbol.
*         example: "Sewage drain"
*         required: true
*       - in: DTA_DESC
*         name: DTA_DESC
*         description: The description of the symbol.
*         example: "A symbol that represents a sewage drain."
*         required: false
*       - in: DTA_UID
*         name: DTA_UID
*         description: The unique 6 digit code of the symbol.
*         example: 123456
*         required: false
*       - in: DTA_USR
*         name: DTA_USR
*         description: The contact of the user suggesting the symbol.
*         example: "example.mail@gmail.com"
*         required: false
*       - in: DTA_SVG
*         name: DTA_SVG
*         description: The ".svg" file containing the symbol sketch. 
*         example: "123456.svg"
*         required: false
*       - in: DTA_SIZE_SVG
*         name: DTA_SIZE_SVG
*         description: The ".svg" file containing the dimensions of the symbol sketch. 
*         example: "123456.svg"
*         required: false
*       - in: DTA_DWG
*         name: DTA_DWG
*         description: The ".dwg" file containing the symbol sketch. 
*         example: "123456.dwg"
*         required: false
*       - in: DTA_SHP
*         name: DTA_SHP
*         description: The ".shp" file containing the symbol sketch. 
*         example: "123456.shp"
*         required: false
*       - in: DTA_LIN
*         name: DTA_LIN
*         description: The ".lin" file containing the symbol sketch. 
*         example: "123456.lin"
*         required: false
*       - in: DTA_ATT
*         name: DTA_ATT
*         description: An attachement file representing the symbol (a .jpg, .png or .svg).
*         example: "123455.jpg"
*         required: false
*   responses:
*       "201":
*           description: "Symbol added."
*       "400":
*           description: "Error. Incorrect input."
*       "401":
*           description: "You must be logged in to add active symbols."
*       "500":
*           description: "Error. Database server error."
*/

router.post('/', authenticationMiddleware.verify, async function(req,res) {
    if (req.body) {
        let valid = true;
        // validate metadata
        if (! (!req.body.UID_DTA)) valid = false;
        req.body.UID_DTA = uuid.v4();
        
        if (! (!req.body.UID_TYP || /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/.test(req.body.UID_TYP))) valid = false;  
        if (! (!req.body.UID_CLR || /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/.test(req.body.UID_CLR))) valid = false;
        if (! (!req.body.UID_CAT || /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/.test(req.body.UID_CAT))) valid = false;
        if (! typeof req.body.DTA_TXT === 'string') valid = false;
        if (! (!req.body.DTA_DESC || typeof req.body.DTA_DESC === 'string')) valid = false;
        if (! (!req.body.DTA_UID || /[0-9]{6}$/.test(req.body.DTA_UID))) valid = false;
        if (! (!req.body.DTA_USR || typeof req.body.DTA_USR === 'string' || typeof req.body.DTA_USR === 'number')) valid = false; 
        
        if (! (!req.body.hasOwnProperty('DTA_CUTS_CONTOURS') || (req.body.DTA_CUTS_CONTOURS == 'true' || req.body.DTA_CUTS_CONTOURS == 'false'))) valid = false;

        if ((!req.user || req.user.DTA_ACT == false) && req.body.DTA_ACT == true) res.status(401).send("You must be logged in to add active symbols.");
        else req.body.DTA_ACT = false;
        
        if (!valid) { res.status(400).send('Error. Incorrect input.'); return; }

        // validate files
        if (req.files) {
            if (! (!req.files.DTA_SVG || req.files.DTA_SVG.mimetype === 'image/svg+xml')) valid = false;
            if (! (!req.files.DTA_SIZE_SVG || req.files.DTA_SIZE_SVG.mimetype)) valid = false;
            if (! (!req.files.DTA_PNG || req.files.DTA_PNG.mimetype === 'image/png')) valid = false;
            if (! (!req.files.DTA_DWG || req.files.DTA_DWG.mimetype === 'image/vnd.dwg')) valid = false;
            if (! (!req.files.DTA_LIN || req.files.DTA_LIN.mimetype === 'application/octet-stream')) valid = false;
            if (! (!req.files.DTA_SHP || req.files.DTA_SHP.mimetype === 'application/octet-stream')) valid = false; 

            const allowedAttachementMimetypes = ['image/png', 'image/jpeg', 'image/svg+xml'];
            if (! (!req.files.DTA_ATT || allowedAttachementMimetypes.includes(req.files.DTA_ATT.mimetype))) valid = false;

            if (!valid) { res.status(400).send('Error. Incorrect input.'); return; }
            else {
                if (req.files.DTA_SLD) {
                    req.files.DTA_SLD.mv(`./static/sld/${req.body.UID_DTA}.sld`);
                    req.body.DTA_SLD = `./sld/${req.body.UID_DTA}.sld`;
                }
                if (req.files.DTA_SVG) {
                    req.files.DTA_SVG.mv(`./static/svg/${req.body.UID_DTA}.svg`);
                    req.body.DTA_SVG = `./svg/${req.body.UID_DTA}.svg`;
                }
                if (req.files.DTA_SIZE_SVG) {
                    req.files.DTA_SIZE_SVG.mv(`./static/size_svg/${req.body.UID_DTA}.svg`);
                    req.body.DTA_SIZE_SVG = `./size_svg/${req.body.UID_DTA}.svg`;
                }
                if (req.files.DTA_PNG) {
                    req.files.DTA_PNG.mv(`./static/png/${req.body.UID_DTA}.png`);
                    req.body.DTA_PNG = `./png/${req.body.UID_DTA}.png`;
                }
                if (req.files.DTA_SHP) {
                    req.files.DTA_SHP.mv(`./static/shp/${req.body.UID_DTA}.shp`);
                    req.body.DTA_SHP = `./shp/${req.body.UID_DTA}.shp`;
                }
                if (req.files.DTA_DWG) {
                    req.files.DTA_DWG.mv(`./static/dwg/${req.body.UID_DTA}.dwg`);
                    req.body.DTA_DWG = `./dwg/${req.body.UID_DTA}.dwg`;
                }
                if (req.files.DTA_LIN) {
                    req.files.DTA_LIN.mv(`./static/lin/${req.body.UID_DTA}.lin`);
                    req.body.DTA_LIN = `./lin/${req.body.UID_DTA}.lin`;
                }
                if (req.files.DTA_ATT) {
                    const extension = path.extname(req.files.DTA_ATT.name);
                    req.files.DTA_ATT.mv(`./static/attachments/${req.body.UID_DTA}${extension}`);
                    req.body.DTA_ATT = `./attachments/${req.body.UID_DTA}${extension}`;
                } 
            }
        }


        await dataBaseInterface.SQL_FILE_CS('./DataBase/Queries/symbols/add_new.sql', [req.body], (err, result) => {
            if (err) res.status(500).send('Error. Database server error.');
            else res.status(201).send("Symbol added.");
        });
    }
    else res.status(400).send('Error. Incorrect input.');
});

/**
* @swagger
* /symbols/{uuid}:
*  put:
*   tags: [Symbols]
*   description: Updates a symbol.
*   consumes:
*       - "multipart/form-data"
*   parameters:
*       - in: UID_TYP
*         name: UID_TYP
*         description: The uuid of the symbol type.
*         example: b2dbc2de-2ad9-47bb-ba86-dc6c0c8fbe28
*         required: false
*       - in: UID_CLR
*         name: UID_CLR
*         description: The uuid of the symbol color.
*         example: b2dbc2de-2ad9-47bb-ba86-dc6c0c8fbe28
*         required: false
*       - in: UID_CAT
*         name: UID_CAT
*         description: The uuid of the symbol category.
*         example: b2dbc2de-2ad9-47bb-ba86-dc6c0c8fbe28
*         required: false
*       - in: DTA_TXT
*         name: DTA_TXT
*         description: The name of the symbol.
*         example: "Sewage drain"
*         required: true
*       - in: DTA_DESC
*         name: DTA_DESC
*         description: The description of the symbol.
*         example: "A symbol that represents a sewage drain."
*         required: false
*       - in: DTA_UID
*         name: DTA_UID
*         description: The unique 6 digit code of the symbol.
*         example: 123456
*         required: false
*       - in: DTA_SVG
*         name: DTA_SVG
*         description: The ".svg" file containing the symbol sketch. 
*         example: "123456.svg"
*         required: false
*       - in: DTA_DWG
*         name: DTA_DWG
*         description: The ".dwg" file containing the symbol sketch. 
*         example: "123456.dwg"
*         required: false
*       - in: DTA_SHP
*         name: DTA_SHP
*         description: The ".shp" file containing the symbol sketch. 
*         example: "123456.shp"
*         required: false
*       - in: DTA_LIN
*         name: DTA_LIN
*         description: The ".lin" file containing the symbol sketch. 
*         example: "123456.lin"
*         required: false
*       - in: DTA_PNG
*         name: DTA_PNG
*         description: The ".png" file containing the symbol sketch. 
*         example: "123456.png"
*         required: false
*       - in: DTA_CUTS_CONTOURS
*         name: DTA_CUTS_CONTOURS
*         description: A variable that determines whether or not contours should be drawn over the type. False by default.
*         example: "true/false"
*         required: false
*   responses:
*       "201":
*           description: "Symbol updated."
*       "400":
*           description: "Error. Incorrect input."
*       "401":
*           description: "You must be logged in to edit active symbols."
*       "500":
*           description: "Error. Database server error."
*/
router.put('/:uuid', authenticationMiddleware.verify, async function(req,res) {
    if (req.body) {
        let valid = true;

        if (! (req.body.DTA_UID == undefined || req.body.DTA_UID == null || /[0-9]{6}$/.test(req.body.DTA_UID))) valid = false;
        if (!valid) { res.status(400).send('Error. Incorrect input.'); return; }

        if (req.files) {
            if (! (!req.files.DTA_SVG || req.files.DTA_SVG.mimetype === 'image/svg+xml')) valid = false;
            if (! (!req.files.DTA_PNG || req.files.DTA_PNG.mimetype === 'image/png')) valid = false;
            if (! (!req.files.DTA_DWG || req.files.DTA_DWG.mimetype === 'image/vnd.dwg')) valid = false;
            if (! (!req.files.DTA_LIN || req.files.DTA_LIN.mimetype === 'application/octet-stream')) valid = false;
            if (! (!req.files.DTA_SHP || req.files.DTA_SHP.mimetype === 'application/octet-stream')) valid = false; 
            if (! (!req.body.hasOwnProperty('DTA_CUTS_CONTOURS') || (req.body.DTA_CUTS_CONTOURS == 'true' || req.body.DTA_CUTS_CONTOURS == 'false'))) valid = false;

            if (!valid) { res.status(400).send('Error. Incorrect input.'); return; }
            else {
                await dataBaseInterface.SQL_FILE_CS('./DataBase/Queries/symbols/get_single.sql', req.params.uuid, (err, result) => {
                    if (err) res.status(500).send("Error. Failed to get data.");
                    else req.body.UID_DTA = result.rows[0].UID_DTA;
                });

                if (req.files.DTA_SVG) {
                    req.files.DTA_SVG.mv(`./static/svg/${req.body.UID_DTA}.svg`);
                    req.body.DTA_SVG = `./svg/${req.body.UID_DTA}.svg`;
                }
                if (req.files.DTA_SLD) {
                    req.files.DTA_SLD.mv(`./static/sld/${req.body.UID_DTA}.sld`);
                    req.body.DTA_SLD = `./sld/${req.body.UID_DTA}.sld`;
                }
                if (req.files.DTA_SIZE_SVG) {
                    req.files.DTA_SIZE_SVG.mv(`./static/size_svg/${req.body.UID_DTA}.svg`);
                    req.body.DTA_SIZE_SVG = `./size_svg/${req.body.UID_DTA}.svg`;
                }
                if (req.files.DTA_PNG) {
                    req.files.DTA_PNG.mv(`./static/png/${req.body.UID_DTA}.png`);
                    req.body.DTA_PNG = `./png/${req.body.UID_DTA}.png`;
                }
                if (req.files.DTA_SHP) {
                    req.files.DTA_SHP.mv(`./static/shp/${req.body.UID_DTA}.shp`);
                    req.body.DTA_SHP = `./shp/${req.body.UID_DTA}.shp`;
                }
                if (req.files.DTA_DWG) {
                    req.files.DTA_DWG.mv(`./static/dwg/${req.body.UID_DTA}.dwg`);
                    req.body.DTA_DWG = `./dwg/${req.body.UID_DTA}.dwg`;
                }
                if (req.files.DTA_LIN) {
                    req.files.DTA_LIN.mv(`./static/lin/${req.body.UID_DTA}.lin`);
                    req.body.DTA_LIN = `./lin/${req.body.UID_DTA}.lin`;
                }
            }
        }

        await dataBaseInterface.SQL_FILE_US('./DataBase/Queries/symbols/update.sql', [req.body, req.params.uuid], (err, result) => {
            if (err) res.status(500).send('Error. Database server error.');
            else res.status(201).send("Symbol updated.");
        });
    }
    else res.status(400).send('Error. Incorrect input.');
})

/**
* @swagger
* /symbols/{uuid}:
*  get:
*   tags: [Symbols]
*   description: Gets a single symbol by uuid.
*   parameters:
*       - in: uuid
*         name: uuid
*         description: The uuid of the symbol.
*         example: b2dbc2de-2ad9-47bb-ba86-dc6c0c8fbe28
*   responses:
*       "200":
*           description: "Symbol retrieved."
*       "400":
*           description: "Error. Incorrect input."
*       "500":
*           description: "Database server error."
*/

router.get('/:uuid', async function(req,res) {
    if (! /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/.test(req.params.uuid)) 
        { res.status(400).send("Error. Incorrect input."); return; }
    else {  
        await dataBaseInterface.SQL_FILE_CS('./DataBase/Queries/symbols/get_single.sql', req.params.uuid, (err, result) => {
            if (err) res.status(500).send("Error. Failed to get data.");
            else res.status(200).json(result.rows[0]);
        });
    }
})

/**
* @swagger
* /symbols/by_code/{code}:
*  get:
*   tags: [Symbols]
*   description: Gets a single symbol by its six digit code.
*   parameters:
*       - in: code
*         name: code
*         description: The six digit code of the symbol.
*         example: 123456
*   responses:
*       "200":
*           description: "Symbol retrieved."
*       "400":
*           description: "Error. Incorrect input."
*       "500":
*           description: "Database server error."
*/
router.get('/by_code/:code', async function(req,res) {
    if (! /[0-9]{6}$/.test(req.params.code)) 
        { res.status(400).send("Error. Incorrect input."); return; }
    else {  
        await dataBaseInterface.SQL_FILE_CS('./DataBase/Queries/symbols/get_single_by_code.sql', req.params.code, (err, result) => {
            if (err) res.status(500).send("Error. Failed to get data.");
            else res.status(200).json(result.rows[0]);
        });
    }
})

/**
* @swagger
* /symbols/by_type/{type}:
*  get:
*   tags: [Symbols]
*   description: Gets symbols by their type. (T,L,O,P).
*   parameters:
*       - in: type
*         name: type
*         description: The type of symbol the user is earching for.
*         example: T or L or O or P.
*   responses:
*       "200":
*           description: "Symbol retrieved."
*       "400":
*           description: "Error. Incorrect input."
*       "500":
*           description: "Database server error."
*/
router.get('/by_type/:type', async function(req,res) {
    if (req.params.type == 'T' || req.params.type == 'L' || req.params.type == 'O' || req.params.type == 'P') {
        await dataBaseInterface.SQL_FILE_CS('./DataBase/Queries/symbols/get_by_type.sql', req.params.type, (err, result) => {
            if (err) res.status(500).send("Error. Failed to get data.");
            else res.status(200).json(result.rows);
        });
    }
    else res.status(400).send("Error. Incorrect input.");
})

/**
* @swagger
* /symbols/{uuid}:
*  put:
*   tags: [Symbols]
*   description: Updates a particular symbol with data. Requires the user to be logged in.
*   parameters:
*       - in: uuid
*         name: uuid
*         description: The uuid of the symbol.
*         example: b2dbc2de-2ad9-47bb-ba86-dc6c0c8fbe28
*       - in: UID_TYP
*         name: UID_TYP
*         description: The uuid of the symbol type.
*         example: b2dbc2de-2ad9-47bb-ba86-dc6c0c8fbe28
*         required: false
*       - in: UID_CLR
*         name: UID_CLR
*         description: The uuid of the symbol color.
*         example: b2dbc2de-2ad9-47bb-ba86-dc6c0c8fbe28
*         required: false
*       - in: UID_CAT
*         name: UID_CAT
*         description: The uuid of the symbol category.
*         example: b2dbc2de-2ad9-47bb-ba86-dc6c0c8fbe28
*         required: false
*       - in: DTA_TXT
*         name: DTA_TXT
*         description: The name of the symbol.
*         example: "Sewage drain"
*         required: true
*       - in: DTA_DESC
*         name: DTA_DESC
*         description: The description of the symbol.
*         example: "A symbol that represents a sewage drain."
*         required: false
*       - in: DTA_UID
*         name: DTA_UID
*         description: The unique 6 digit code of the symbol.
*         example: 123456
*         required: false
*       - in: DTA_USR
*         name: DTA_USR
*         description: The contact of the user suggesting the symbol.
*         example: "example.mail@gmail.com"
*         required: false
*       - in: DTA_SVG
*         name: DTA_SVG
*         description: The ".svg" file containing the symbol sketch. 
*         example: "123456.svg"
*         required: false
*       - in: DTA_PNG
*         name: DTA_PNG
*         description: The ".png" file containing the symbol sketch. 
*         example: "123456.png"
*         required: false
*       - in: DTA_SIZE_SVG
*         name: DTA_SIZE_SVG
*         description: The ".svg" file containing the dimensions of the symbol sketch. 
*         example: "123456.svg"
*         required: false
*       - in: DTA_DWG
*         name: DTA_DWG
*         description: The ".dwg" file containing the symbol sketch. 
*         example: "123456.dwg"
*         required: false
*       - in: DTA_SHP
*         name: DTA_SHP
*         description: The ".shp" file containing the symbol sketch. 
*         example: "123456.shp"
*         required: false
*       - in: DTA_LIN
*         name: DTA_LIN
*         description: The ".lin" file containing the symbol sketch. 
*         example: "123456.lin"
*         required: false
*       - in: DTA_ATT
*         name: DTA_ATT
*         description: An attachement file representing the symbol (a .jpg, .png or .svg).
*         example: "123455.jpg"
*         required: false
*       - in: DTA_CUTS_CONTOURS
*         name: DTA_CUTS_CONTOURS
*         description: A variable that determines whether or not contours should be drawn over the type. False by default.
*         example: "true/false"
*         required: false
*   responses:
*       "201":
*           description: "Symbol updated."
*       "400":
*           description: "Error. Incorrect input."
*       "401":
*           description: "Error. You must be logged in to edit symbols."
*       "500":
*           description: "Database server error."
*/

router.post('/accept/:uuid', authenticationMiddleware.verify, async function(req,res) {
    if (! /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/.test(req.params.uuid)) {
        res.status(400);
        res.send("Error. Incorrect input. UUID")
        return;
    }
    else {  
        if (req.user && req.user.DTA_ACT) {
            if (req.body) {
                let currentData = await dataBaseInterface.SQL_FILE_CS('./DataBase/Queries/symbols/get_single.sql', req.params.uuid);
                if (!currentData || currentData.length == 0) {
                    res.status(500).send('Database server error.'); return;
                }

                currentData = currentData[0];

                let valid = true;

                // if (! (!req.body.UID_DTA)) valid = false;
                if (! (!req.body.UID_TYP || /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/.test(req.body.UID_TYP))) valid = false;  
                if (! (!req.body.UID_CLR || /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/.test(req.body.UID_CLR))) valid = false;
                if (! (!req.body.UID_CAT || /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/.test(req.body.UID_CAT))) valid = false;
                if (! (!req.body.DTA_TXT || typeof req.body.DTA_TXT === 'string')) valid = false;
                if (! (!req.body.DTA_DESC || typeof req.body.DTA_DESC === 'string')) valid = false;
                if (! (!req.body.hasOwnProperty('DTA_CUTS_CONTOURS') || (req.body.DTA_CUTS_CONTOURS == 'true' || req.body.DTA_CUTS_CONTOURS == 'false'))) valid = false;
                // if (! (!req.body.DTA_USR || typeof req.body.DTA_USR === 'string' || typeof req.body.DTA_USR === 'number')) valid = false;
                if (! (!req.body.DTA_ACT || typeof req.body.DTA_ACT === 'string')) valid = false;
                if (! (!req.body.DTA_UID || /[0-9]{6}$/.test(req.body.DTA_UID))) valid = false;

                if (!valid) {
                    res.status(400).send('Error. Incorrect input. First'); return;
                }

                // validate files
                if (req.files) {
                    if (! (!req.files.DTA_SVG || (req.files.DTA_SVG.mimetype === 'image/svg+xml'))) valid = false;
                    if (! (!req.files.DTA_PNG || (req.files.DTA_PNG.mimetype === 'image/png'))) valid = false;
                    // if (! (!req.files.DTA_SIZE_SVG || (req.files.DTA_SIZE_SVG.mimetype === 'image/svg+xml'))) valid = false;
                    if (! (!req.files.DTA_DWG || (req.files.DTA_DWG.mimetype === 'application/octet-stream'))) valid = false;
                    // if (! (!req.files.DTA_LIN || (req.files.DTA_LIN.mimetype === 'application/octet-stream'))) valid = false;
                    if (! (!req.files.DTA_SHP || (req.files.DTA_SHP.mimetype === 'application/octet-stream'))) valid = false;
                    
                    /* const allowedAttachementMimetypes = ['image/png', 'image/jpeg', 'image/svg+xml'];
                    if (! (!req.files.DTA_ATT || allowedAttachementMimetypes.includes(req.files.DTA_ATT.mimetype))) valid = false; */
                }

                if (!valid) {
                    res.status(400).send('Error. Incorrect input. Second'); return;
                }
                else {
                    if (req.files) {
                        if (req.files.DTA_SVG) {
                            req.files.DTA_SVG.mv(`./static/svg/${currentData.UID_DTA}.svg`);
                            req.body.DTA_SVG = `./svg/${currentData.UID_DTA}.svg`;
                        }
                        if (req.files.DTA_SIZE_SVG) {
                            req.files.DTA_SIZE_SVG.mv(`./static/size_svg/${currentData.UID_DTA}.svg`);
                            req.body.DTA_SIZE_SVG = `./size_svg/${currentData.UID_DTA}.svg`;
                        }
                        if (req.files.DTA_PNG) {
                            req.files.DTA_PNG.mv(`./static/png/${currentData.UID_DTA}.png`);
                            req.body.DTA_PNG = `./png/${currentData.UID_DTA}.png`;
                        }
                        if (req.files.DTA_SHP) {
                            req.files.DTA_SHP.mv(`./static/shp/${currentData.UID_DTA}.shp`);
                            req.body.DTA_SHP = `./shp/${currentData.UID_DTA}.shp`;
                        }
                        if (req.files.DTA_DWG) {
                            req.files.DTA_DWG.mv(`./static/dwg/${currentData.UID_DTA}.dwg`);
                            req.body.DTA_DWG = `./dwg/${currentData.UID_DTA}.dwg`;
                        }
                        if (req.files.DTA_LIN) {
                            req.files.DTA_LIN.mv(`./static/lin/${currentData.UID_DTA}.lin`);
                            req.body.DTA_LIN = `./lin/${currentData.UID_DTA}.lin`;
                        }
                        if (req.files.DTA_SLD) {
                            req.files.DTA_SLD.mv(`./static/sld/${currentData.UID_DTA}.sld`);
                            req.body.DTA_SLD = `./sld/${currentData.UID_DTA}.sld`;
                        }
                        if (req.files.DTA_ATT) {
                            const extension = path.extname(req.files.DTA_ATT.name);
                            req.files.DTA_ATT.mv(`./static/attachments/${currentData.UID_DTA}${extension}`);
                            req.body.DTA_ATT = `./attachments/${currentData.UID_DTA}${extension}`;
                        }
                    }

                    await dataBaseInterface.SQL_FILE_US('./DataBase/Queries/symbols/update.sql', [req.body,req.params.uuid], (err, result) => {
                        if (err) { res.status(500).send('Database server error.'); return; }
                        else {
                            res.status(201).send("Symbol updated.");
                        }
                    });
                }
            }
        }
        else res.status(401).send("You must be logged in to edit symbols.");
    }
});

/**
* @swagger
* /symbols/{uuid}:
*  delete:
*   tags: [Symbols]
*   description: Deletes a particular symbol. Requires the user to be logged in.
*   parameters:
*       - in: uuid
*         name: uuid
*         description: The uuid of the symbol.
*         example: b2dbc2de-2ad9-47bb-ba86-dc6c0c8fbe28
*   responses:
*       "200":
*           description: "Symbol deleted."
*       "400":
*           description: "Error. Incorrect input."
*       "401":
*           description: "Error. You must be logged in to delete symbols."
*       "500":
*           description: "Database server error."
*/

router.delete('/:uuid', authenticationMiddleware.verify, async function(req,res) {
    if (req.user.DTA_ACT) {
        if (! /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/.test(req.params.uuid)) {
            res.status(401);
            res.send("Error. Incorrect input.")
            return;
        }
        else {
            await dataBaseInterface.SQL_FILE_CS('./DataBase/Queries/symbols/delete.sql', req.params.uuid, (err, result) => {
                if (err) {
                    res.status(500);
                    res.send("Database server error.");
                }
                else {
                    res.status(200);
                    res.send("Symbol deleted.");
                }
            });
        }
    }
    else res.status(400).send("You must be logged in to delete symbols.");
});



module.exports = router;