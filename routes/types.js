let express = require('express');
const router = express.Router();
const dataBaseInterface = require('../DataBase/Connector/DataBase.js');

/**
 * @swagger
 * tags:
 *   name: Types
 *   description: Get symbol types
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     Type:
 *       type: object
 *       properties:
 *          UID_DTA:
 *           type: uuid
 *           description: A uuid identifying the type.
 *           example: b2dbc2de-2ad9-47bb-ba86-dc6c0c8fbe28
 *          DTA_TXT:
 *           type: string
 *           description: The name of the type.
 *           example: Fences
 */

/**
* @swagger
* /types:
*  get:
*   tags: [Types]
*   description: Gets all types.
*   responses:
*       "200":
*           description: "The types were found."
*           content:
*               application/json:
*                   schema:
*                       type: object
*                       $ref: '#/components/schemas/Type'
*       "500":
*           description: "Error. Database server error."
*/
router.get('/', async (req,res) => {
   await dataBaseInterface.SQL_FILE_CS('./DataBase/Queries/types/get_all.sql', undefined, (err, result) => {
       if (err) res.status(500).send('Error. Database server error.');
       else res.status(200).json(result.rows);
   });
});

/**
* @swagger
* /types/{uuid}:
*  get:
*   tags: [Types]
*   description: Gets a single type by uuid.
*   responses:
*       "200":
*           description: "The type was found."
*           content:
*               application/json:
*                   schema:
*                       type: object
*                       $ref: '#/components/schemas/Type'
*       "400":
*           description: "Error. Incorrect input."
*       "500":
*           description: "Error. Failed to get data."
*/
router.get('/:uuid', async (req,res) => {
    if (! /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/.test(req.params.uuid)) 
    { res.status(400).send("Error. Incorrect input."); return; }
    else {
        await dataBaseInterface.SQL_FILE_CS('./DataBase/Queries/types/get_single.sql', req.params.uuid, (err, result) => {
            if (err) res.status(500).send('Error. Database server error.');
            else res.status(200).json(result.rows[0]);
        });
    }
 });

module.exports = router;