let express = require('express');
const router = express.Router();
var path = require('path');

const authenticationMiddleware = require('../authentication/middleware.js');

function SendFile (REQ, RES, T, F)
{
    RES.sendFile(T, { root: path.resolve() } );
}

router.get('/', (req, res) => { 
    if (req.session.visitedPageBefore == true) { res.redirect('/WEB/main?REDI=SUB/Symbols'); } 
    else {
        req.session.visitedPageBefore = true;
        res.redirect('/WEB/welcome');
    }
});

router.get('/WEB/main',     (req, res) => { SendFile(req, res, './static/views/WEB/main.html', '/SUB/Error'); });
router.get('/WEB/login',    authenticationMiddleware.verify, (req, res) => {
    if (req.user) { res.redirect('/WEB/main?REDI=SUB/Symbols'); }
    else { SendFile(req, res, './static/views/WEB/login.html', '/SUB/Error'); }
});
router.get('/WEB/register', authenticationMiddleware.verify, (req, res) => { 
    if (req.user) { res.redirect('/WEB/main?REDI=SUB/Symbols'); }
    else { SendFile(req, res, './static/views/WEB/register.html', '/SUB/Error'); }
});
router.get('/WEB/logout',   authenticationMiddleware.verify, (req, res) => { 
    if (req.user) { res.redirect('/WEB/main?REDI=SUB/Symbols'); }
    else { res.redirect('/WEB/main?REDI=SUB/Symbols'); }
});

router.get('/SUB/Error',             (req, res) => { SendFile(req, res, './static/views/SUB/Error.html',             '/SUB/Error'); });
router.get('/WEB/register',          (req, res) => { SendFile(req, res, './static/views/WEB/register.html',          '/SUB/Error'); });
router.get('/WEB/welcome',           (req, res) => { SendFile(req, res, './static/views/WEB/welcome.html',           '/SUB/Error'); });
router.get('/SUB/Symbols',           (req, res) => { SendFile(req, res, './static/views/SUB/Symbols.html',           '/SUB/Error'); });
router.get('/SUB/AccountManagement', (req, res) => { SendFile(req, res, './static/views/SUB/AccountManagement.html', '/SUB/Error'); });
router.get('/SUB/Draw',              (req, res) => { SendFile(req, res, './static/views/SUB/Draw.html',              '/SUB/Error'); });
router.get('/SUB/Admin',             (req, res) => { SendFile(req, res, './static/views/SUB/Admin.html',             '/SUB/Error'); });
router.get('/SUB/Pdf',               (req, res) => { SendFile(req, res, './static/views/SUB/Pdf.html',               '/SUB/Error'); });

module.exports = router;