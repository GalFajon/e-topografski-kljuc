let express = require('express');
const router = express.Router();
const dataBaseInterface = require('../DataBase/Connector/DataBase.js');
const fs = require("fs");

/**
* @swagger
* /pdf/:
*  get:
*   tags: [Pdf]
*   description: Generates the HTML markup of the pdf file, containing all symbols.
*   responses:
*       "201":
*           description: "Returns a string containing the markup."
*/
router.get('/', async (req,res) => {
    await dataBaseInterface.SQL_FILE_CS('./DataBase/Queries/symbols/get_for_pdf.sql', undefined, async (err, result) => {
        if (err) res.status(500).send('Error. Database server error.');
        else {
            const symbols = result.rows;
            
            html = `
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
            <style>  
                .blank {
                    border: 1px solid white;
                }

               .blank td {
                    border: 1px solid white;
                }

                table {
                    margin-top: 30px;

                    background-color:white; 
                    color:black; 
                    width:700px;
                    height:800px;

                    margin-left: 45px;

                    break-inside: avoid-page;
                    page-break-after: always;
                }
                
                h2 {
                    margin-left: 45px;
                }
    
                tr {
                    border: 1px solid black;
                    margin:10px;
                }
    
                th {
                    border: 1px solid black;
                    margin: 1px;
                    height: 5px;
                }
    
                td {
                    border: 1px solid black;
                    margin:10px;
                    position:relative;
                    width:120px; 
                    height:100px;
    
                    text-align:center;
                }
                svg {
                    position:absolute;
                    left:0; 
                    top:0; 
    
                    width:100%;
                    height:100%;
    
                    display:block;
                    margin:auto;
                }
    
                @page {
                    size:a4;
                    margin:1cm;
                }
            </style>
            <div>
            `;
            
            let symbolsOnPage = 0;
            let previousCategory = symbols[0].DTA_CAT;
            let pageHeading = '';

            for (let i=0; i < symbols.length; i++) {
                let symbol = symbols[i];
                let nextSymbol = undefined;

                if (i < (symbols.length - 1))
                    nextSymbol = symbols[i + 1];

                let svg = '';
                let size_svg = '';

                if (nextSymbol) {
                    pageHeading = nextSymbol.DTA_CAT;
                }

                if (symbolsOnPage == 0) {
                    html += `<h2>${pageHeading}</h2>`;
                    html += `
                        <table>
                        <tr>
                            <th>Šifra</th>
                            <th>Simbol</th>
                            <th>Izris</th>
                            <th>Tip</th>
                            <th>Barva</th>
                            <th>Opis</th>
                        </tr>
                    `;
                }

                if (symbol.DTA_SIZE_SVG) {
                    try {
                        size_svg = fs.readFileSync(`./static/${symbol.DTA_SIZE_SVG.substring(2)}`, {encoding:'utf8'});
                    } 
                    catch (err) {
                        size_svg = " ";
                    }
                } 

                if (symbol.DTA_SVG) {
                    try {
                        svg = fs.readFileSync(`./static/${symbol.DTA_SVG.substring(2)}`, {encoding:'utf8'});
                    } 
                    catch (err) {
                        svg = " ";
                    }
                }

                if (symbol.DTA_UID == undefined) symbol.DTA_UID = '';
                if (symbol.DTA_TXT == undefined) symbol.DTA_TXT = '';
                if (symbol.DTA_CLR == undefined) symbol.DTA_CLR = '';
                if (symbol.DTA_TYP == undefined) symbol.DTA_TYP = '';
                if (symbol.DTA_DESC == undefined) symbol.DTA_DESC = '';
                if (symbol.DTA_CAT == undefined) symbol.DTA_CAT = '';
                
                html += `
                    <tr>
                        <td>${symbol.DTA_UID}</td>
                        <td>${symbol.DTA_TXT}</td>
                        <td>${svg}</td>
                        <td>${symbol.DTA_TYP}</td>
                        <td style="font-size: 12px; color: rgb(${symbol.DTA_R},${symbol.DTA_G},${symbol.DTA_B});">RGB: ${symbol.DTA_R}, ${symbol.DTA_G}, ${symbol.DTA_B}</td>
                        <td>${symbol.DTA_DESC}</td>
                    </tr>
                `;

                symbolsOnPage++;

                if (symbolsOnPage == 9 || (nextSymbol && nextSymbol.DTA_CAT != previousCategory)) { 
                    while (symbolsOnPage < 9) {
                        html += `
                        <tr class="blank">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        `;

                        symbolsOnPage += 1;
                    }
                    
                    html += `
                        </table>
                    `;

                    if (nextSymbol && nextSymbol.DTA_CAT)
                        previousCategory = nextSymbol.DTA_CAT;

                    symbolsOnPage = 0;
                }
            }

            html += '</table></div>';
            res.status(201).send(html)
        }
    });
});

module.exports = router;