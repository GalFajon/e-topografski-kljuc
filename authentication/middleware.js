const dataBaseInterface = require('../DataBase/Connector/DataBase.js');

exports.verify = (req,res,next) => {
    if (req.session.userid) {
        dataBaseInterface.SQL_FILE_US('./DataBase/Queries/users/get_by_uuid.sql', [req.session.userid], (error, result) => {
            let userData = result.rows[0];
            if (userData) {
                req.user = {
                    UID_DTA: userData.UID_DTA,
                    DTA_ACT: userData.DTA_ACT,
                    DTA_EML: userData.DTA_EML
                };
            }
            else {
                req.user = undefined;
            }
            next();
        });
   }
   else {
       req.user = undefined;
       next();
   }
}