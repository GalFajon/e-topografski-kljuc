# E_Topografski_kljuc:
## How to use:
Node.js setup:
- Run the "npm install" command.

Postgres setup:
- Create the database and database user:
    CREATE DATABASE "E-TOPOGRAFSKI-KLJUC" WITH ENCODING = 'UTF8';

    DROP USER IF EXISTS "E-TOPOGRAFSKI-KLJUC";
    CREATE USER "E-TOPOGRAFSKI-KLJUC" WITH ENCRYPTED PASSWORD 'E-TOPOGRAFSKI-KLJUC';
    ALTER ROLE "E-TOPOGRAFSKI-KLJUC" SUPERUSER;
    GRANT ALL PRIVILEGES ON DATABASE "E-TOPOGRAFSKI-KLJUC" TO "E-TOPOGRAFSKI-KLJUC";

- Import UUID generation extension:
    CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
    
- Import password cryptography extension:
    CREATE EXTENSION pgcrypto;

Finally, run "node Main.js" in the command line.