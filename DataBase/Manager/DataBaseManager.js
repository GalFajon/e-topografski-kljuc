// CREATE DB
// ---------
/*
    CREATE DATABASE "E-TOPOGRAFSKI-KLJUC" WITH ENCODING = 'UTF8';
*/

// CREATE USER
// -----------
/*
    DROP USER IF EXISTS "E-TOPOGRAFSKI-KLJUC";

    CREATE USER "E-TOPOGRAFSKI-KLJUC" WITH ENCRYPTED PASSWORD 'E-TOPOGRAFSKI-KLJUC';

    ALTER ROLE "E-TOPOGRAFSKI-KLJUC" SUPERUSER;

    GRANT ALL PRIVILEGES ON DATABASE "E-TOPOGRAFSKI-KLJUC" TO "E-TOPOGRAFSKI-KLJUC";
*/

var PGD = require("../Connector/DataBase.js");
var sprintf = require('sprintf-js').sprintf;
var FIO = require('fs');

async function PreformUpdate()
{
    while(true)
    {
        var SQL = await PGD.SQL_TEXT_GS(`SELECT * FROM "E-TOPOGRAFSKI-KLJUC"."DTB_INF_DTA" WHERE "DTA_KEY" = 'VER'`);
        var VER = SQL ? parseInt(SQL.DTA_VAL) : 0; // Is Schema does not exists, start Step 1
        
        var UpdateFileName = sprintf("./DataBase/Step/V%05d.sql", (VER + 1));
        if (FIO.existsSync(UpdateFileName))
        {
            console.log(sprintf("Updating PostGIS DB: Step %s", UpdateFileName));

            await PGD.SQL_FILE_CS(UpdateFileName, null);
        }
        else { console.log("DataBase Update Done!"); break; }
    }
}

async function importOldSymbols() {
    const SQL = await PGD.SQL_TEXT_GS(`SELECT * FROM "E-TOPOGRAFSKI-KLJUC"."DTB_INF_DTA" WHERE "DTA_KEY" = 'IMPORTED'`);

    if (SQL.DTA_VAL == "FALSE") {
        await PGD.SQL_FILE_CS('./DataBase/Queries/import/import.sql', undefined);

        await PGD.SQL_FILE_CS('./DataBase/Queries/symbols/get_all.sql', undefined, async (err,results) => {
            if (err) console.log("Error while importing old symbols")
            else if (results) {
                for (let symbol of results.rows) {
                    if (FIO.existsSync(`./static/svg/${symbol.DTA_UID}.svg`)) {
                        await PGD.SQL_TEXT_GS(`UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_SVG" = './svg/${symbol.DTA_UID}.svg' WHERE "DTA_UID" = '${symbol.DTA_UID}'`);
                    }
                    else console.log(`Svg file not found for symbol ${symbol.DTA_UID}.`);

                    if (FIO.existsSync(`./static/dwg/${symbol.DTA_UID}.dwg`)) {
                        await PGD.SQL_TEXT_GS(`UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_DWG" = './dwg/${symbol.DTA_UID}.dwg' WHERE "DTA_UID" = '${symbol.DTA_UID}'`);
                    }
                    else console.log(`Dwg file not found for symbol ${symbol.DTA_UID}.`);

                    if (FIO.existsSync(`./static/shp/${symbol.DTA_UID}.shp`)) {
                        await PGD.SQL_TEXT_GS(`UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_SHP" = './shp/${symbol.DTA_UID}.shp' WHERE "DTA_UID" = '${symbol.DTA_UID}'`);
                    }
                    else console.log(`Shp file not found for symbol ${symbol.DTA_UID}.`);
                    
                    if (FIO.existsSync(`./static/png/${symbol.DTA_UID}.png`)) {
                        await PGD.SQL_TEXT_GS(`UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_PNG" = './png/${symbol.DTA_UID}.png' WHERE "DTA_UID" = '${symbol.DTA_UID}'`);
                    }
                    else console.log(`Png file not found for symbol ${symbol.DTA_UID}.`);

                }

                await PGD.SQL_TEXT_GS(`UPDATE "E-TOPOGRAFSKI-KLJUC"."DTB_INF_DTA" SET "DTA_VAL" = 'TRUE' WHERE "DTA_KEY" = 'IMPORTED'`);
                console.log("Imported old symbols.");
            }
        });
    }
}

module.exports = { PreformUpdate, importOldSymbols };