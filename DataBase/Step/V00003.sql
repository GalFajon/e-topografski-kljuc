/* This update fixes certain isues with the initial SYM_DTB table. */

INSERT INTO "E-TOPOGRAFSKI-KLJUC"."SYM_TYP" ("UID_DTA", "DTA_TXT") VALUES 
    ( uuid_generate_v4(), 'T' ),
    ( uuid_generate_v4(), 'L' ),
    ( uuid_generate_v4(), 'O' ),
    ( uuid_generate_v4(), 'P' )
;
/* Točkovni, Linijski, Opisni, Ploskovni */


ALTER TABLE "E-TOPOGRAFSKI-KLJUC"."SYM_CLR" RENAME COLUMN "DTA_CLR" TO "DTA_TXT";

ALTER TABLE "E-TOPOGRAFSKI-KLJUC"."SYM_CLR" ADD COLUMN "DTA_R" int CHECK ("DTA_R" >= 0 AND "DTA_R" <= 255);
ALTER TABLE "E-TOPOGRAFSKI-KLJUC"."SYM_CLR" ADD COLUMN "DTA_G" int CHECK ("DTA_G" >= 0 AND "DTA_G" <= 255);
ALTER TABLE "E-TOPOGRAFSKI-KLJUC"."SYM_CLR" ADD COLUMN "DTA_B" int CHECK ("DTA_B" >= 0 AND "DTA_B" <= 255);

INSERT INTO "E-TOPOGRAFSKI-KLJUC"."SYM_CLR" ("UID_DTA", "DTA_TXT", "DTA_R", "DTA_G", "DTA_B") VALUES
    ( uuid_generate_v4(), 'črna', 0, 0, 0 ),
    ( uuid_generate_v4(), 'magenta', 255, 0, 255 ),
    ( uuid_generate_v4(), 'temno modra', 0, 0, 255 ),
    ( uuid_generate_v4(), 'svetlo modra', 0, 127, 255 ),
    ( uuid_generate_v4(), 'olivno zelena', 0, 127, 0 ),
    ( uuid_generate_v4(), 'rdeča', 255, 0, 0 ),
    ( uuid_generate_v4(), 'oranžna', 255, 127, 0 ),
    ( uuid_generate_v4(), 'rumena', 255, 255, 0 ),
    ( uuid_generate_v4(), 'vijolična', 127, 0, 255 ),
    ( uuid_generate_v4(), 'cyan modra', 0, 255, 255 ),
    ( uuid_generate_v4(), 'rjava', 200, 150, 0 ),
    ( uuid_generate_v4(), 'siva', 150, 150, 150 )
;


CREATE TABLE "E-TOPOGRAFSKI-KLJUC"."SYM_CAT" (
   "UID_DTA" uuid DEFAULT uuid_generate_v4(),
   "DTA_TXT" text,

   PRIMARY KEY("UID_DTA")
);

INSERT INTO "E-TOPOGRAFSKI-KLJUC"."SYM_CAT" ("UID_DTA", "DTA_TXT") VALUES
    ( '15016f03-82f0-47ab-a845-8d9e4a844635', 'GEODETSKE TOČKE'),
    ( '23a2ef8c-45f9-4816-a4fd-c34a8cb28b2a', 'MEJE'),
    ( '0527644f-bfda-4faf-8098-c8244f57373f', 'STAVBE IN GRADBENO INŽENIRSKI OBJEKTI'),
    ( '9bacf928-70d2-4263-aac1-dd55edb5094c', 'NARAVNI ELEMENTI TOPOGRAFIJE'),
    ( '6faa5cb3-980c-486b-87c3-d41cca6e04d6', 'ZEMLJEPISNA IMENA IN NAPISI'),
    ( '50103e46-a46a-11ec-b909-0242ac120002', 'DODATNO')
;

ALTER TABLE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" ADD COLUMN "UID_CAT" uuid;

ALTER TABLE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" 
ADD FOREIGN KEY("UID_CAT") REFERENCES "E-TOPOGRAFSKI-KLJUC"."SYM_CAT"("UID_DTA") ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" ADD COLUMN "DTA_DESC" text;

ALTER TABLE "E-TOPOGRAFSKI-KLJUC"."USR_DTB" DROP COLUMN "UID_TYP";
DROP TABLE "E-TOPOGRAFSKI-KLJUC"."USR_TYP";

UPDATE "E-TOPOGRAFSKI-KLJUC"."DTB_INF_DTA" SET "DTA_VAL" = 3 WHERE "DTA_KEY" = 'VER';