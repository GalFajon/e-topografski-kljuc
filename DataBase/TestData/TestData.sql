INSERT INTO "E-TOPOGRAFSKI-KLJUC"."SYM_CAT" ("DTA_TXT") VALUES 
    ('Communal buildings'),
    ('Private buildings'),
    ('Vegetation');

INSERT INTO "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" ("UID_TYP", "UID_CLR", "DTA_TXT", "DTA_UID", "DTA_ACT", "DTA_USR", "DTA_SVG", "DTA_DWG", "DTA_SHP", "DTA_LIN", "UID_CAT", "DTA_DESC")  
    SELECT 
        (SELECT "UID_DTA" FROM "E-TOPOGRAFSKI-KLJUC"."SYM_TYP" WHERE "DTA_TXT" = 'T'), 
        (SELECT "UID_DTA" FROM "E-TOPOGRAFSKI-KLJUC"."SYM_CLR" WHERE "DTA_TXT" = 'black'),
        'Sewage processing plant',
        '110050',
        TRUE,
        'gla.nojaf@gmail.com',
        '110050.svg',
        '110050.dwg',
        '110050.shp',
        '110050.lin',
        (SELECT "UID_DTA" FROM "E-TOPOGRAFSKI-KLJUC"."SYM_CAT" WHERE "DTA_TXT" = 'Communal buildings'), 
        'A symbol that represents something.'
    ;

INSERT INTO "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" ("UID_TYP", "UID_CLR", "DTA_TXT", "DTA_UID", "DTA_ACT", "DTA_USR", "DTA_SVG", "DTA_DWG", "DTA_SHP", "DTA_LIN", "UID_CAT", "DTA_DESC")  
    SELECT 
        (SELECT "UID_DTA" FROM "E-TOPOGRAFSKI-KLJUC"."SYM_TYP" WHERE "DTA_TXT" = 'O'), 
        (SELECT "UID_DTA" FROM "E-TOPOGRAFSKI-KLJUC"."SYM_CLR" WHERE "DTA_TXT" = 'yellow'),
        'Bush',
        '1234567',
        FALSE,
        'user@email.com',
        '1234567.svg',
        '1234567.dwg',
        '1234567.shp',
        '1234567.lin',
        (SELECT "UID_DTA" FROM "E-TOPOGRAFSKI-KLJUC"."SYM_CAT" WHERE "DTA_TXT" = 'Vegetation'), 
        'A symbol that represents something.'
    ;

INSERT INTO "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" ("UID_TYP", "UID_CLR", "DTA_TXT", "DTA_UID", "DTA_ACT", "DTA_USR", "DTA_SVG", "DTA_DWG", "DTA_SHP", "DTA_LIN", "UID_CAT", "DTA_DESC")  
    SELECT 
        (SELECT "UID_DTA" FROM "E-TOPOGRAFSKI-KLJUC"."SYM_TYP" WHERE "DTA_TXT" = 'L'), 
        (SELECT "UID_DTA" FROM "E-TOPOGRAFSKI-KLJUC"."SYM_CLR" WHERE "DTA_TXT" = 'magenta'),
        'Apartment building',
        '45678901',
        TRUE,
        'gla.nojaf@gmail.com',
        '45678901.svg',
        '45678901.dwg',
        '45678901.shp',
        '45678901.lin',
        (SELECT "UID_DTA" FROM "E-TOPOGRAFSKI-KLJUC"."SYM_CAT" WHERE "DTA_TXT" = 'Private buildings'), 
        'A symbol that represents something.'
    ;

INSERT INTO "E-TOPOGRAFSKI-KLJUC"."USR_DTB" ("DTA_EML", "DTA_PSW", "DTA_ACT") VALUES 
    ('gla.nojaf@gmail.com', crypt('geslo123', gen_salt('bf')), TRUE),
    ('tanja.vanja@yahoo.si', crypt('geslo123', gen_salt('bf')), TRUE),
    ('trudek.stankovski@siol.net', crypt('geslo123', gen_salt('bf')), FALSE)
;

