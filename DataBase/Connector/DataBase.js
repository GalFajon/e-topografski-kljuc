var { Pool } = require('pg');
var SqlString = require('sqlstring');
var FIO = require('fs');
const express = require('express');
const app = express();

var DEBUG_TXT = false;

var PGD_C = new Pool
({
    user: Settings_PG.USER,
    host: Settings_PG.HOST,
    database: Settings_PG.DATA,
    password: Settings_PG.PASS,
    port: Settings_PG.PORT,
    max: 25
});

PGD_C.on('error', (err, client) =>
{
    console.log('DataBase connection error : ' + err);
});

async function SQL_FILE_CS(SQL, DTA, callback)
{   
    try {
        const RES = await GET(SqlString.format(FIO.readFileSync(SQL, 'UTF8'), JSON.stringify(DTA)).replaceAll("\\", "").replaceAll("`", "\"").replaceAll("\'\"", "\'").replaceAll("\"\'", "\'"), callback);
        return RES;
    }
    catch (error) {
        console.log("Error getting file: '" + SQL + "' " + error);
    }
}

async function SQL_FILE_US(SQL, DTA, callback)
{
    var RES = await GET(SqlString.format(FIO.readFileSync(SQL, 'UTF8'), DTA).replaceAll("\\", "").replaceAll("`", "\""), callback);

    return RES && RES.length === 1 ? RES[0] : null;
}

async function SQL_TEXT_GS(SQL) { var RES = await GET(SQL); return RES && RES.length === 1 ? RES[0] : null; } // SINGLE

async function GET(SQL, callback)
{
    if(DEBUG_TXT) { console.log(SQL); }

    var RES = await new Promise((resolve, reject) =>
    {
        PGD_C.connect((err, client, release) =>
        {
            if (err) { reject(err); return; }

            client.query(SQL, (err, result) =>
            {
                release();

                if (callback) {
                    callback(err, result);
                }

                if (err) { 
                    reject(err);
                    return; 
                }

                resolve(result.rows);
            });
        });
    }).catch(e => { console.log(e); });

    return RES;
}

module.exports = { SQL_TEXT_GS, SQL_FILE_CS, SQL_FILE_US, GET };