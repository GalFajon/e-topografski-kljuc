/* Base import: */
INSERT INTO "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"("DTA_ACT", "DTA_UID","DTA_TXT", "UID_CAT", "UID_TYP")
VALUES 
('TRUE',110010,'Temeljna geodetska točka', '15016f03-82f0-47ab-a845-8d9e4a844635', NULL),
('TRUE',110020,'Temeljna geodetska točka z določenimi ETRS koordinatami', '15016f03-82f0-47ab-a845-8d9e4a844635', NULL),
('TRUE',110030,'Izmeritvena geodetska točka', '15016f03-82f0-47ab-a845-8d9e4a844635', NULL),
('TRUE',110040,'Izmeritvena geodetska točka z določenimi ETRS koordinatami', '15016f03-82f0-47ab-a845-8d9e4a844635', NULL),
('TRUE',110050,'Permanentna GPS postaja', '15016f03-82f0-47ab-a845-8d9e4a844635', NULL),
('TRUE',120010,'Fundamentalni reper', '15016f03-82f0-47ab-a845-8d9e4a844635', NULL),
('TRUE',120020,'Reper', '15016f03-82f0-47ab-a845-8d9e4a844635', NULL),
('TRUE',130010,'Absolutna gravimetrična točka', '15016f03-82f0-47ab-a845-8d9e4a844635', NULL),
('TRUE',130020,'Relativna gravimetrična točka', '15016f03-82f0-47ab-a845-8d9e4a844635', NULL),
('TRUE',220010,'Mejno znamenje', '23a2ef8c-45f9-4816-a4fd-c34a8cb28b2a', NULL),
('TRUE',220011,'Naravni kamen', '23a2ef8c-45f9-4816-a4fd-c34a8cb28b2a', NULL),
('TRUE',220012,'Betonski mejnik', '23a2ef8c-45f9-4816-a4fd-c34a8cb28b2a', NULL),
('TRUE',220013,'Mejnik s kovinskim sidrom', '23a2ef8c-45f9-4816-a4fd-c34a8cb28b2a', NULL),
('TRUE',220014,'Kovinski čep ali klin', '23a2ef8c-45f9-4816-a4fd-c34a8cb28b2a', NULL),
('TRUE',220015,'Vklesan križ', '23a2ef8c-45f9-4816-a4fd-c34a8cb28b2a', NULL),
('TRUE',220016,'Neoznačena točka', '23a2ef8c-45f9-4816-a4fd-c34a8cb28b2a', NULL),
('TRUE',220040,'Meja parcele', '23a2ef8c-45f9-4816-a4fd-c34a8cb28b2a', NULL),
('TRUE',220050,'Urejena meja parcele', '23a2ef8c-45f9-4816-a4fd-c34a8cb28b2a', NULL),
('TRUE',311010,'Stanovanjska stavba', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',312010,'Poslovna stavba', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',312011,'Okvir stavbe', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',312020,'Gospodarska stavba Zidana garaža', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',312030,'Manjša ali odprta gosp. stavba, garaža, baraka', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',312040,'Samostojna streha', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',312050,'Okvir stavbe nad zemljiščem', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',312060,'Okvir stavbe pod zemljiščem', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',312070,'Kozolec', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',312080,'Dvojni kozolec', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',312090,'Cerkev', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',312100,'Samostojni zvonik', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',312110,'Mrliška vežica', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',312120,'Razvalina - območje', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',313010,'Širok nadstrešek', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',313020,'Arkade, balkoni (spodaj prehodni)', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',313030,'Prizidek - vetrolov, zimski vrt...', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',313040,'Vodoravna plošča ali terasa', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',313050,'Stopnice', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',313060,'Dimnik', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',313070,'Dimnik', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',313080,'Nosilni steber stavbe s pravok. prerezom', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',313090,'Nos. steber stavbe z okroglim prerezom', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',313100,'Steber', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',313110,'Jašek za svetl., kurjavo, tov. dvigalo', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',313120,'Zračniki - ploskovni', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',321010,'Jašek komunalnih vodov - Okrogel', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',321020,'Jašek komunalnih vodov - Pravokoten', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',321030,'Konec voda', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',322010,'Vodovodni jašek - Okrogel', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',322020,'Vodovodni jašek - Pravokoten', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',322030,'Zasun, zapirač', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',322040,'Nadzemni hidrant', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',322050,'Podzemni hidrant', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',322060,'Vodovod', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',323010,'Kanalski jašek - Okrogel', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',323020,'Kanalski jašek - Pravokoten', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',323030,'Kanalizacija za padavinske (meteorne) vode', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',323040,'Kanalizacija za odpadne vode', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',324010,'Elektro jašek - Okrogel', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',324020,'Elektro jašek - Pravokoten', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',324030,'Drog za el. Vod nizke napetosti', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',324040,'Električna konzola', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',324050,'Stojalo za elektriko', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',324060,'Drog za el. Vod visoke napetosti', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',324070,'Predalčni steber za el. Vod vis.nap.', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',324080,'Predalčni steber za električni vod visoke napetosti', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',324090,'Transformator na drogu', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',324100,'Manjša trafo postaja', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',324110,'Manjša razdelilna postaja', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',324120,'Trafo postaja', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',324130,'Razdelilna postaja', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',324140,'Razdelilna trafo postaja', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',324150,'Električna omarica', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',324160,'Tablica za električni vod', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',324170,'Elektrika nizka napetost', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',324180,'Elektrika visoka napetost', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',325010,'Telefonski jašek - Okrogel', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',325020,'Telefonski jašek - Pravokoten', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',325030,'Telefonski drog', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',325040,'Telefonski vod', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',325051,'Telefonska omarica', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',326010,'Plinski zapirač', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',326020,'Magistralni plinovod - opozorilna tabla', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',326030,'Magistralni plinovod - duhalna cev', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',326040,'Plinovod', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',327010,'Nadz. vod tople vode ali pare', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',327020,'Cevovod za toplo vodo ali paro', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',327030,'Obod nadzemnega cevovoda tople vode ali pare', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',328010,'Jašek javne razsvetljave - okrogel', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',328020,'Jašek javne razsvetljave - pravokoten', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',328030,'Svetilka na drogu', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',328040,'Svetilka (Dekorativna)', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',328050,'Javna razsvetljava', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',329060,'Kabelska kanalizacija', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',329090,'Elektronske komunikacije', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330010,'Kovinski most', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330020,'Betonski most', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330030,'Leseni most', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330040,'Viseči most', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330050,'Brv', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330060,'Propust', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330070,'Večji prepust', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330080,'Jarek ob cesti ali železnici', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330090,'Podhod', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330100,'Nadhod nad cesto, železnico', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330110,'Znak za kilometražo', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330120,'Cesta, ulica, trg', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330130,'Pot', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330140,'Opuščena cesta, Kolovoz', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330150,'Kolovoz', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330160,'Steza', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330170,'Kolesarska steza', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330180,'Bankina -utrjena ali neutrjena', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330190,'Pločnik, raven ali nagnjen, rampa', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330200,'Parkirišče', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330210,'Semafor', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330220,'Požiralnik – okrogli', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330230,'Požiralnik - oglati', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330240,'Požiralnik - cestni pod robnikom', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330250,'Peskolov, jašek požiralnika', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330260,'Rešetke na tleh', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330270,'Območje železniške proge', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330280,'Železniška proga', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330290,'Industrijski tiri, tiri na postaji', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330300,'Elektrificirana proga', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330310,'Železniški svetlobni signal', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330320,'Zapornica', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330330,'Kretnica', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330340,'Drog – nosilec konzole za električni vod', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330350,'Tirni zaključek', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330360,'Sidrišče', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330370,'Svetilnik', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330380,'Privez', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330390,'Pomol', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',330400,'Prometni znak - kvadratni', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',340010,'Vhod v rudniško jamo', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',340020,'Rudniški jašek', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',340030,'Prezračevalni jašek', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',340040,'Stalni žerjav', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',351010,'Zidana ograja', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',351020,'Ograja iz zloženega kamenja', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',351030,'Živa meja', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',351040,'Ograja', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',351050,'Op. in podp. zid - trikotnika na koncih', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',351060,'Oporni zid - brez trik. na koncih linije', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',351070,'Oporni zid - trikotnik na koncu linije', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',351080,'Oporni zid - trikotnik na začetku linije', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',351090,'Poševna zaščitna ploskev (ograja)', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',352010,'Versko znamenje, kapelica', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',352020,'Osamljen grob', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',352030,'Manjši spomenik s temeljno ploščo', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',352040,'Spomenik s temeljno ploščo', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',353010,'Drog - lesen, betonski, kovinski', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',353011,'Stebriček', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',353020,'Steber - predalčni', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',353030,'Stolp - zidani', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',353040,'Steber videokamere', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',353050,'Antenski stolp', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',354010,'Bunker', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',354020,'Hranilnik - vode, plina, nafte, bencina', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',354030,'Ograjena gnojna jama', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',354040,'Cvetličnjak', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',354050,'Zid podporni, okrasni', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',354060,'Korito za rože, grmovnice', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',354061,'Korito za rože, cvetlice', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',354070,'Manjša zgradba nedef. namena', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',354080,'Manjša zgradba (telefon)', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',354090,'Manjša zgradba (stražnica)', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',354100,'Manjša zgradba (prometna kabina)', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',354110,'Manjša zgradba (kiosk)', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',354120,'Kiosk', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',354130,'Mostna tehtnica', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',354140,'Športno igrišče', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',354150,'Otroško igrišče', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',354151,'Peskovnik', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',354160,'Tribuna', '0527644f-bfda-4faf-8098-c8244f57373f', NULL),
('TRUE',410010,'Stalni vodotok', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',410020,'Stalni nar. vodotok - ožji od 0.5 mm v načrtu', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',410030,'Nestalni vodotok', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',410040,'Nestalni nar. vodotok - ožji od 0.5 mm v načrtu', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',410060,'Stalni umetni vodotok, kanal', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',410070,'Kanal, ožji od 0.5 mm v načrtu', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',410080,'Jarek z nestalno vodo', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',410090,'Vodno korito', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',410100,'Stalna stoječa voda', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',410110,'Nestalna stoječa voda', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',410120,'Bazeni, bajerji, večji vodotoki', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',410130,'Prehodna in neprehodna močvirja, soline', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',410140,'Izvir (orientacija)', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',410150,'Presihajoč izvir', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',410160,'Izvir termalne vode', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',410170,'Predalčni steber z zbiralnikom - pravokoten', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',410180,'Predalčni steber z zbiralnikom - okrogel', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',410190,'Prizemni zbiralnik', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',410200,'Zajetje izvira', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',410210,'Cisterna z vodo (kapnica)', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',410220,'Vodnjak', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',410230,'Vodomet, okrasni vodnjak', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',410240,'Ponikev', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',410250,'Javni iztok vode (pipa, vodnjak)', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',410260,'Izliv zajezene vode', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',410270,'Slap', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',410280,'Odbijač vode', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',410290,'Odbijač vode, ožji od 0.5 mm v načrtu', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',410300,'Odbijač vode, ožji od 0.5 mm v načrtu', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',410310,'Vodni prag', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',410320,'Ravni vrhnji del jezu', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',410330,'Pregrada nad 3m višine', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',410340,'Nagnjen del zemeljske pregrade', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',410350,'Nagnjen del kamnite pregrade', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',410360,'Vodna zapornica', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',410370,'Vodna zapornica - zgradba', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',410380,'Vodomerna letev', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',420040,'Pobočje, reber, brežina nad 2m višine', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',420050,'Pobočje, reber, brežina do 2m višine', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',420060,'Osamljena skala', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',420070,'Čer', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',420080,'Skalnati rob', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',420090,'Greben (skalnat)', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',420100,'Greben (zemlja)', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',420110,'Skale, pečine, stene', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',420120,'Kamenje, grušč, melišče, Sipina', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',420130,'Zemeljski plaz', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',420140,'Skalna razpoka (točkovni znak)', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',420150,'Dno globeli, vdolbine na terenu', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',421010,'Rob brezna, širšega od 5 m', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',421020,'Kraško brezno', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',421030,'Brezno - ožje od 5 m', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',421040,'Kraški izvir', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',421050,'Kraška jama, podzemna jama', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',421060,'Žlebiči', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',421070,'Vrtača do 5 m z neizr. robom', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',421080,'Vrtača do 5 m z izr. robom', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',431010,'Listnato drevo, sadno', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',431020,'Iglasto drevo', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',431030,'Značilno iglasto drevo', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',431040,'Značilno listnato ali sadno drevo', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',431050,'Grm', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',431060,'Grmovje v vrsti', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',432010,'Njiva', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',432020,'Manjša njiva (vrt)', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',432021,'Cvetlična greda', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',432030,'Grmovje', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',432040,'Manjše grmovje', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',432050,'Oljkov nasad', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',432060,'Manjši oljkov nasad', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',432070,'Gozdna preseka', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',432080,'Sadovnjak', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',432090,'Vinograd', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',432100,'Hmeljišče', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',432110,'Travnik', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',432120,'Manjši travnik', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',432130,'Zelenica', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',432140,'Manjša zelenica', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',432150,'Park', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',432160,'Trstičje, ločje', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',432170,'Manjše trstičje, ločje', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',432180,'Listnati gozd', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',432190,'Iglasti gozd', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',432200,'Mešani gozd', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',432210,'Drevesnica', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',433010,'Dvorišče', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',433011,'Tlak (asfaltirane, peščene, tlakovane površine)', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',433020,'Neplodno, degradirano območje', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',433030,'Gradbišče', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',433040,'Pokopališče', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',433050,'Manjše pokopališče', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',433060,'Grobišče', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',433070,'Manjše grobišče', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',433080,'Arheološko najdbišče', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',433090,'Odlagališče smeti', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',433100,'Gramoznica', '9bacf928-70d2-4263-aac1-dd55edb5094c', NULL),
('TRUE',600008,'Puščica', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',600009,'Prečni profil', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',610001,'Sleme', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',610002,'Vhod', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',610003,'Uvoz', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',610004,'Smer', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',610005,'Vhod LD', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',610006,'Rob strehe', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',610009,'Meja pridobivalnega prostora', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',610010,'Horizontalna signalizacija', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',610011,'Pogreznjen robnik', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',610012,'Ležeči policaj, grbina', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',610013,'Prometna tabla, reklamna tabla', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',610014,'Jeklena varnostna ograja (JVO)', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',611001,'Jašek z uro', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',611002,'Hišni zapirač', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',611003,'Zračnik', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',611004,'Peskolov - pravokoten', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',611005,'Jašek strelovoda', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',612001,'Telefonska omarica', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',612002,'Kabelska televizija', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',612003,'Plinska omarica', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',612004,'JR omarica', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',612005,'Radarska omarica', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',612006,'Stebriček katodne zaščite', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',612007,'Lovilec olj', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',612008,'Lovilec olj', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',612009,'Ponikovalnica', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',612010,'Ponikovalnica', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',612011,'Sidro - merilno mesto', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',612012,'Čistilna naprava', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',612013,'Mala čistilna naprava', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',612014,'Vodno zajetje - vrtina', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',612015,'Greznica', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',612016,'Greznica', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',612017,'Željeno mesto priklopa kanalizacije', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',612018,'e - polnilna postaja', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',613001,'Požarna omarica', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',613002,'Omarica', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',613003,'Projektirana točka', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',613004,'Posedalna plošča', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',618888,'Prometni znak', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',619996,'CityLight', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',619997,'Bicikelj', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',619998,'Urbanomat', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',619999,'Parkomat', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',620020,'Nadzemni vod', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',630001,'Listopadno drevo do 5m', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',630002,'Listopadno drevo od 5 do 10m', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',630003,'Listopadno drevo nad 10m', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',630004,'Vedno zeleno drevo do 5m', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',630005,'Vedno zeleno drevo od 5 do 10m', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',630006,'Vedno zeleno drevo nad 10m', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',630007,'Posamezen grm', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',630008,'Plinjak', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',630009,'Brezno', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',630010,'Blatni izpust', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',640001,'Vedno zelena živa meja do 0.8m', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',640002,'Vedno zelena živa meja od 0.8m do 1.5m', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',640003,'Vedno zelena živa meja nad 1.5m', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',640004,'Listopadna živa meja od 0.8m do 1.5m', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',640005,'Listopadna živa meja od 0.8m do 1.5m', '50103e46-a46a-11ec-b909-0242ac120002', NULL),
('TRUE',640006,'Listopadna živa meja nad 1.5m', '50103e46-a46a-11ec-b909-0242ac120002', NULL);

/* Type update: */
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '110010';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '110020';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '110030';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '110040';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '110050';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '120010';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = 'Riše se pravokotno na objekt'
WHERE "DTA_UID" = '120020';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '130010';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '130020';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = 'Debelina linije 0.2 mm'
WHERE "DTA_UID" = '210010';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = 'Debelina linije 0.2 mm'
WHERE "DTA_UID" = '210020';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = 'Debelina linije 0.2 mm'
WHERE "DTA_UID" = '210030';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '210040';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = 'Debelina linije 0.3 mm'
WHERE "DTA_UID" = '210060';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = 'Debelina linije 0.3 mm'
WHERE "DTA_UID" = '210070';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '220010';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '220011';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '220012';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '220013';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '220014';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '220015';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '220016';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = 'Debelina linije 0.2 mm'
WHERE "DTA_UID" = '220040';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = 'Debelina linije 0.3 mm'
WHERE "DTA_UID" = '220050';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = 'Izris oboda 0.3 mm'
WHERE "DTA_UID" = '311010';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = 'Izris oboda 0.3 mm'
WHERE "DTA_UID" = '312010';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = 'Okvir stavbe 0.3 mm'
WHERE "DTA_UID" = '312011';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = 'Izris oboda 0.3 mm'
WHERE "DTA_UID" = '312020';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = 'Izris oboda 0.3 mm'
WHERE "DTA_UID" = '312030';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = 'Izris oboda 0.3 mm'
WHERE "DTA_UID" = '312040';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '312050';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '312060';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '312070';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = 'Izris oboda 0.3 mm'
WHERE "DTA_UID" = '312080';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = 'Izris oboda 0.3 mm'
WHERE "DTA_UID" = '312090';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = 'Izris oboda 0.3 mm'
WHERE "DTA_UID" = '312100';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = 'Izris oboda 0.3 mm'
WHERE "DTA_UID" = '312110';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = 'Izris oboda 0.3 mm z linijo 312050'
WHERE "DTA_UID" = '312120';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = 'Izris oboda 0.3 mm z linijo 312051'
WHERE "DTA_UID" = '313010';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = 'Izris oboda 0.3 mm z linijo 312052'
WHERE "DTA_UID" = '313020';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = 'Izris oboda 0.3 mm'
WHERE "DTA_UID" = '313030';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '313040';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '313050';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '313060';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = 'Izris oboda 0.5 mm'
WHERE "DTA_UID" = '313070';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '313080';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '313090';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '313100';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '313110';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '313120';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '321010';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '321020';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '321030';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '322010';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '322020';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '322030';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '322040';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '322050';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '322060';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '323010';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '323020';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = 'Možnost prikaza smeri padca kanalizacije'
WHERE "DTA_UID" = '323030';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = 'Možnost prikaza smeri padca kanalizacije'
WHERE "DTA_UID" = '323040';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '324010';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '324020';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '324030';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = 'Usmerjen pravokotno na steno objekta'
WHERE "DTA_UID" = '324040';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '324050';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '324060';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '324070';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '324080';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '324090';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '324100';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '324110';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = 'Izris oboda 0.3 mm'
WHERE "DTA_UID" = '324120';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = 'Izris oboda 0.3 mm'
WHERE "DTA_UID" = '324130';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = 'Izris oboda 0.3 mm'
WHERE "DTA_UID" = '324140';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '324150';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '324160';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '324170';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '324180';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '325010';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '325020';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '325030';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '325040';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '325051';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '326010';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '326020';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '326030';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '326040';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = 'Izris oboda 0.3 mm z linijo 327030'
WHERE "DTA_UID" = '327010';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '327020';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '327030';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '328010';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '328020';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '328030';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '328040';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '328050';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '329060';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '329090';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330010';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330020';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330030';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330040';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330050';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330060';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330070';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330080';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330090';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330100';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330110';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = 'Izris oboda 0.2 mm'
WHERE "DTA_UID" = '330120';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330130';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = 'Izris oboda z linijo 330150'
WHERE "DTA_UID" = '330140';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330150';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330160';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330170';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330180';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330190';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330200';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330210';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330220';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330230';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330240';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330250';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330260';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330270';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330280';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330290';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330300';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330310';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330320';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330330';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330340';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330350';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330360';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330370';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330380';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330390';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '330400';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '340010';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '340020';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '340030';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '340040';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '351010';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '351020';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '351030';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '351040';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '351050';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '351060';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '351070';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '351080';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '351090';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '352010';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '352020';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '352030';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '352040';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '353010';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '353011';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '353020';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '353030';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '353040';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '353050';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = 'Izris oboda 0.3 mm'
WHERE "DTA_UID" = '354010';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = 'Izris oboda 0.3 mm'
WHERE "DTA_UID" = '354020';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '354030';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '354040';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '354050';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '354060';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '354061';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '354070';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '354080';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '354090';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '354100';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '354110';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = 'Izris oboda 0.3 mm'
WHERE "DTA_UID" = '354120';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '354130';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '354140';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '354150';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '354151';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '354160';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = 'Usmerjen v smeri vodnega toka, izris oboda z linijo 410050'
WHERE "DTA_UID" = '410010';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '410020';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '410030';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '410040';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = 'Linija debeline 0.1 mm'
WHERE "DTA_UID" = '410050';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = 'Usmerjen v smeri vodnega toka, izris oboda z linijo 410050'
WHERE "DTA_UID" = '410060';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '410070';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '410080';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = 'Usmerjen v smeri vodnega toka, izris oboda z linijo 410050'
WHERE "DTA_UID" = '410090';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = 'Izris oboda z linijo 410050'
WHERE "DTA_UID" = '410100';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '410110';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = 'Izris oboda z linijo 410050'
WHERE "DTA_UID" = '410120';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '410130';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = 'Usmerjen v smeri vodnega toka'
WHERE "DTA_UID" = '410140';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = 'Usmerjen v smeri vodnega toka'
WHERE "DTA_UID" = '410150';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '410160';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '410170';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '410180';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = 'Izris oboda z linijo 410050'
WHERE "DTA_UID" = '410190';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '410200';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '410210';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '410220';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '410230';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '410240';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '410250';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '410260';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '410270';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '410280';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '410290';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '410300';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '410310';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '410320';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '410330';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '410340';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '410350';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '410360';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = 'Izris oboda 0.3 mm'
WHERE "DTA_UID" = '410370';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '410380';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '420040';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '420050';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '420060';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '420070';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '420080';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '420090';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '420100';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '420110';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '420120';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '420130';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '420140';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '420150';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '421010';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '421020';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '421030';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '421040';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '421050';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '421060';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '421070';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '421080';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '431010';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '431020';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '431030';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '431040';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '431050';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '431060';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '432010';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '432020';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '432021';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '432030';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '432040';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '432050';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '432060';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '432070';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '432080';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '432090';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '432100';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '432110';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '432120';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '432130';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '432140';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '432150';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '432160';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '432170';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '432180';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '432190';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '432200';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '432210';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '433010';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '433011';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '433020';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '433030';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '433040';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '433050';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '433060';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '433070';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '433080';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '433090';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '433100';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '600008';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '600009';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '610001';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '610002';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '610003';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '610004';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '610005';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '610006';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '610009';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '610010';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '610011';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '610012';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '610013';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '610014';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '611001';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '611002';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '611003';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '611004';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '611005';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '612001';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '612002';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '612003';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '612004';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '612005';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '612006';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '612007';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '612008';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '612009';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '612010';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '612011';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '612012';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '612013';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '612014';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '612015';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '612016';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '612017';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '612018';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '613001';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '613002';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '613003';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '613004';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '618888';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '619996';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '619997';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '619998';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '619999';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '620020';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '630001';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '630002';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '630003';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '630004';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '630005';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '630006';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '630007';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '630008';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '630009';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'T'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '630010';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '640001';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '640002';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '640003';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '640004';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '640005';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB"
SET 
    "UID_TYP" = (SELECT "UID_DTA" FROM "SYM_TYP" WHERE "DTA_TXT" = 'L'),
    "DTA_DESC" = ''
WHERE "DTA_UID" = '640006';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='610003';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='353020';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=12, "DTA_G"=162, "DTA_B"=39 WHERE "DTA_UID"='420120';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='321010';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='351050';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='420090';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=198, "DTA_G"=255, "DTA_B"=255 WHERE "DTA_UID"='354140';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='410150';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='330310';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='312070';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='354110';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=102, "DTA_G"=102, "DTA_B"=102 WHERE "DTA_UID"='330130';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='432100';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=104, "DTA_G"=142, "DTA_B"=214 WHERE "DTA_UID"='432170';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='330370';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='410360';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=190, "DTA_G"=252, "DTA_B"=231 WHERE "DTA_UID"='432110';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='330160';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='328010';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='313090';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='410250';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=255, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='410100';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='612016';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=102, "DTA_G"=146, "DTA_B"=148 WHERE "DTA_UID"='354050';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='323020';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='110030';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='612005';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=24, "DTA_G"=203, "DTA_B"=2 WHERE "DTA_UID"='433030';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='613004';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='433070';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='410160';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='130020';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='322040';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='612002';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='340020';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=57, "DTA_G"=205, "DTA_B"=197 WHERE "DTA_UID"='313050';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='420080';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='324010';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='324060';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=128, "DTA_G"=255, "DTA_B"=255 WHERE "DTA_UID"='432021';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=206, "DTA_G"=119, "DTA_B"=215 WHERE "DTA_UID"='313120';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='618888';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='330360';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='330330';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='410140';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='351060';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=98, "DTA_G"=232, "DTA_B"=85 WHERE "DTA_UID"='410130';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=8, "DTA_G"=204, "DTA_B"=148 WHERE "DTA_UID"='313010';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='340030';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='330090';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=190, "DTA_G"=252, "DTA_B"=231 WHERE "DTA_UID"='432120';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='433060';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='324030';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=237, "DTA_G"=152, "DTA_B"=168 WHERE "DTA_UID"='432070';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='330300';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='324040';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=118, "DTA_G"=17, "DTA_B"=56 WHERE "DTA_UID"='420110';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='322020';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='330390';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='410380';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='330110';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=112, "DTA_G"=229, "DTA_B"=5 WHERE "DTA_UID"='312080';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='330150';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=136, "DTA_G"=109, "DTA_B"=140 WHERE "DTA_UID"='410090';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='351020';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='330050';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=67, "DTA_G"=74, "DTA_B"=75 WHERE "DTA_UID"='324130';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='328030';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='324070';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='433040';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='612003';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='351030';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='312060';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='220010';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=4, "DTA_G"=218, "DTA_B"=108 WHERE "DTA_UID"='354060';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='312050';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='613001';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='324150';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='410020';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='410170';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=43, "DTA_G"=59, "DTA_B"=245 WHERE "DTA_UID"='312120';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='410210';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=235, "DTA_G"=37, "DTA_B"=77 WHERE "DTA_UID"='354120';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='421080';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='353011';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='330240';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=24, "DTA_G"=203, "DTA_B"=2 WHERE "DTA_UID"='410060';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=113, "DTA_B"=0 WHERE "DTA_UID"='432200';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='610001';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='612012';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='322010';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=91, "DTA_G"=182, "DTA_B"=240 WHERE "DTA_UID"='313100';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='613002';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=170, "DTA_G"=213, "DTA_B"=255 WHERE "DTA_UID"='432010';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=187, "DTA_G"=47, "DTA_B"=184 WHERE "DTA_UID"='354010';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=170, "DTA_G"=141, "DTA_B"=226 WHERE "DTA_UID"='410280';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=164, "DTA_G"=242, "DTA_B"=169 WHERE "DTA_UID"='420040';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='330230';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='325030';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=90, "DTA_G"=208, "DTA_B"=165 WHERE "DTA_UID"='410350';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='351010';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='410080';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='220015';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='330080';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=58, "DTA_G"=218, "DTA_B"=13 WHERE "DTA_UID"='313020';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=94, "DTA_G"=217, "DTA_B"=70 WHERE "DTA_UID"='354030';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='421010';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='324120';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='433050';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=249, "DTA_G"=184, "DTA_B"=169 WHERE "DTA_UID"='410190';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='611003';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='612004';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=67, "DTA_G"=94, "DTA_B"=153 WHERE "DTA_UID"='410370';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=179, "DTA_G"=124, "DTA_B"=46 WHERE "DTA_UID"='354040';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='421030';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='353050';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=151, "DTA_B"=0 WHERE "DTA_UID"='432180';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='611005';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=9, "DTA_G"=128, "DTA_B"=243 WHERE "DTA_UID"='351090';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='610010';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='619998';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='324080';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='312011';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=128, "DTA_G"=61, "DTA_B"=220 WHERE "DTA_UID"='352040';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=140, "DTA_G"=255, "DTA_B"=198 WHERE "DTA_UID"='432130';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='351080';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=220, "DTA_G"=233, "DTA_B"=181 WHERE "DTA_UID"='330260';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='611002';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='410040';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=250, "DTA_G"=111, "DTA_B"=122 WHERE "DTA_UID"='410030';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='612007';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='431060';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='313060';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=207, "DTA_G"=237, "DTA_B"=214 WHERE "DTA_UID"='421020';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='612018';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=235, "DTA_G"=194, "DTA_B"=215 WHERE "DTA_UID"='330100';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=98, "DTA_G"=232, "DTA_B"=85 WHERE "DTA_UID"='410320';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=185, "DTA_G"=207, "DTA_B"=255 WHERE "DTA_UID"='312010';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='630010';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=85, "DTA_G"=195, "DTA_B"=240 WHERE "DTA_UID"='330010';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='420140';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=54, "DTA_G"=233, "DTA_B"=140 WHERE "DTA_UID"='312100';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='612017';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='325051';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=53, "DTA_G"=190, "DTA_B"=22 WHERE "DTA_UID"='324140';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='340010';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=23, "DTA_G"=178, "DTA_B"=48 WHERE "DTA_UID"='432210';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='328040';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='328020';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=215, "DTA_G"=237, "DTA_B"=179 WHERE "DTA_UID"='312090';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='431010';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=134, "DTA_G"=202, "DTA_B"=80 WHERE "DTA_UID"='420050';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=254, "DTA_G"=19, "DTA_B"=95 WHERE "DTA_UID"='432090';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='432050';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='322030';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='354090';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='612013';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=231, "DTA_G"=164, "DTA_B"=1 WHERE "DTA_UID"='354020';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=255, "DTA_G"=128, "DTA_B"=255 WHERE "DTA_UID"='354061';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='431050';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=127, "DTA_B"=255 WHERE "DTA_UID"='431020';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='220013';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='611004';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='410070';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='619996';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='322050';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='313080';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='324090';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='352010';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='432060';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='420150';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=85, "DTA_G"=85, "DTA_B"=85 WHERE "DTA_UID"='330190';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='351070';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=149, "DTA_G"=184, "DTA_B"=255 WHERE "DTA_UID"='354151';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=220, "DTA_G"=82, "DTA_B"=156 WHERE "DTA_UID"='324120';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=215, "DTA_B"=215 WHERE "DTA_UID"='312030';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='410200';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=211, "DTA_G"=32, "DTA_B"=95 WHERE "DTA_UID"='327010';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='323010';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='420100';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='612011';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='351040';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=140, "DTA_G"=255, "DTA_B"=198 WHERE "DTA_UID"='432140';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='321020';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='410260';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='612010';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=90, "DTA_G"=32, "DTA_B"=165 WHERE "DTA_UID"='410340';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='410220';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=128, "DTA_G"=255, "DTA_B"=255 WHERE "DTA_UID"='311010';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=253, "DTA_G"=227, "DTA_B"=250 WHERE "DTA_UID"='330270';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='410230';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=33, "DTA_G"=31, "DTA_B"=155 WHERE "DTA_UID"='432150';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='110010';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='421070';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='330040';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=33, "DTA_G"=31, "DTA_B"=155 WHERE "DTA_UID"='313070';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=221, "DTA_G"=221, "DTA_B"=221 WHERE "DTA_UID"='330180';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='326020';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='431020';
UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_R"=0, "DTA_G"=0, "DTA_B"=0 WHERE "DTA_UID"='325020';

UPDATE "E-TOPOGRAFSKI-KLJUC"."SYM_DTB" SET "DTA_CUTS_CONTOURS" = TRUE WHERE ("DTA_UID" NOT IN ('988888', '432010', '432020', '432030', '432040', '432050', '432060', '432070', '432080', '432090', '432100', '432110', '432120', '432130', '432140', '432150', '432160', '432170', '432180', '432190', '432200', '432210')) AND "UID_TYP" = (SELECT "UID_TYP" FROM "SYM_TYP" WHERE "DTA_TXT" = 'P');