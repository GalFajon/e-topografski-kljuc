Object.assign(global, { Settings_PG: { "USER": "E-TOPOGRAFSKI-KLJUC", "PASS": "E-TOPOGRAFSKI-KLJUC", "HOST": process.env.DOCKER === "TRUE" ? "postgres" : "localhost", "DATA": "E-TOPOGRAFSKI-KLJUC", "PORT": process.env.DOCKER === "TRUE" ? 5432 : 5433 } });

const express = require('express');
const fileUpload = require('express-fileupload')

const symbolRoutes = require('./routes/symbols.js');
const userRoutes = require('./routes/users.js');
const categoryRoutes = require('./routes/categories.js');
const typeRoutes = require('./routes/types.js');
const webpageRoutes = require('./routes/webpage.js');
const pdfRoutes = require('./routes/pdf.js');

const swaggerUi = require('swagger-ui-express');
const apiSpecs = require('./swagger/specs.js');

const port = 3001;
const session = require('express-session')

const app = express();

const oneDay = 1000 * 60 * 60 * 24;

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  next();
});
app.use(session({
  secret: 'secret',
  resave: false,
  saveUninitialized: true,
  cookie: { maxAge: oneDay }
}))
app.use(
  "/api-docs",
  swaggerUi.serve,
  swaggerUi.setup(apiSpecs)
);
app.use(express.static('./static'))
app.use(express.json());
app.use(fileUpload({
  useTempFiles : true,
  tempFileDir : './static/temp/',
  limits: { 
    fileSize: 100 * 1024 * 1024 // 100 MB
  },
  createParentPath: true,
  safeFileNames: true,
  preserveExtension: Number,
  abortOnLimit: true,
  responseOnLimit: true
}));
app.use(express.static('./files'))

app.use('/symbols', symbolRoutes);
app.use('/users', userRoutes);
app.use('/categories', categoryRoutes);
app.use('/types', typeRoutes);
app.use('/pdf', pdfRoutes);
app.use('/', webpageRoutes);

async function init() {
  await require("./DataBase/Connector/DataBase.js");
  let dbManager = require("./DataBase/Manager/DataBaseManager.js")
  
  await dbManager.PreformUpdate();
  await dbManager.importOldSymbols();
  
  app.listen(port, function() {
    console.log(`Listening on port ${port}!`);
  });
}

init();