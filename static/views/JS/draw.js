/*
 Copyright (c) 2014-2017, Jan Bösenberg & Jürg Lehni

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

var OffsetUtils = {
    offsetPath: function (path, offset, result) {
        var outerPath = new paper.Path({ insert: false }),
            epsilon = paper.Numerical.GEOMETRIC_EPSILON,
            enforeArcs = true;
        for (var i = 0; i < path.curves.length; i++) {
            var curve = path.curves[i];
            if (curve.hasLength(epsilon)) {
                var segments = this.getOffsetSegments(curve, offset),
                    start = segments[0];
                if (outerPath.isEmpty()) {
                    outerPath.addSegments(segments);
                } else {
                    var lastCurve = outerPath.lastCurve;
                    if (!lastCurve.point2.isClose(start.point, epsilon)) {
                        if (enforeArcs || lastCurve.getTangentAtTime(1).dot(start.point.subtract(curve.point1)) >= 0) {
                            this.addRoundJoin(outerPath, start.point, curve.point1, Math.abs(offset));
                        } else {
                            
                            outerPath.lineTo(start.point);
                        }
                    }
                    outerPath.lastSegment.handleOut = start.handleOut;
                    outerPath.addSegments(segments.slice(1));
                }
            }
        }
        if (path.isClosed()) {
            if (!outerPath.lastSegment.point.isClose(outerPath.firstSegment.point, epsilon) && (enforeArcs ||
                outerPath.lastCurve.getTangentAtTime(1).dot(outerPath.firstSegment.point.subtract(path.firstSegment.point)) >= 0)) {
                this.addRoundJoin(outerPath, outerPath.firstSegment.point, path.firstSegment.point, Math.abs(offset));
            }
            outerPath.closePath();
        }
        return outerPath;
    },

    getOffsetSegments: function (curve, offset) {
        if (curve.isStraight()) {
            var n = curve.getNormalAtTime(0.5).multiply(offset),
                p1 = curve.point1.add(n),
                p2 = curve.point2.add(n);
            return [new paper.Segment(p1), new paper.Segment(p2)];
        } else {
            var curves = this.splitCurveForOffseting(curve),
                segments = [];
            for (var i = 0, l = curves.length; i < l; i++) {
                var offsetCurves = this.getOffsetCurves(curves[i], offset, 0),
                    prevSegment;
                for (var j = 0, m = offsetCurves.length; j < m; j++) {
                    var curve = offsetCurves[j],
                        segment = curve.segment1;
                    if (prevSegment) {
                        prevSegment.handleOut = segment.handleOut;
                    } else {
                        segments.push(segment);
                    }
                    segments.push(prevSegment = curve.segment2);
                }
            }
            return segments;
        }
    },

    offsetCurve_middle: function (curve, offset) {
        var v = curve.getValues(),
            p1 = curve.point1.add(paper.Curve.getNormal(v, 0).multiply(offset)),
            p2 = curve.point2.add(paper.Curve.getNormal(v, 1).multiply(offset)),
            pt = paper.Curve.getPoint(v, 0.5).add(
                paper.Curve.getNormal(v, 0.5).multiply(offset)),
            t1 = paper.Curve.getTangent(v, 0),
            t2 = paper.Curve.getTangent(v, 1),
            div = t1.cross(t2) * 3 / 4,
            d = pt.multiply(2).subtract(p1.add(p2)),
            a = d.cross(t2) / div,
            b = d.cross(t1) / div;
        return new paper.Curve(p1, t1.multiply(a), t2.multiply(-b), p2);
    },

    offsetCurve_average: function (curve, offset) {
        var v = curve.getValues(),
            p1 = curve.point1.add(Curve.getNormal(v, 0).multiply(offset)),
            p2 = curve.point2.add(Curve.getNormal(v, 1).multiply(offset)),
            t = this.getAverageTangentTime(v),
            u = 1 - t,
            pt = Curve.getPoint(v, t).add(
                Curve.getNormal(v, t).multiply(offset)),
            t1 = Curve.getTangent(v, 0),
            t2 = Curve.getTangent(v, 1),
            div = t1.cross(t2) * 3 * t * u,
            v = pt.subtract(
                p1.multiply(u * u * (1 + 2 * t)).add(
                    p2.multiply(t * t * (3 - 2 * t)))),
            a = v.cross(t2) / (div * u),
            b = v.cross(t1) / (div * t);
        return new paper.Curve(p1, t1.multiply(a), t2.multiply(-b), p2);
    },

    offsetCurve_simple: function (crv, dist) {
        
        var p1 = crv.point1.add(crv.getNormalAtTime(0).multiply(dist));
        var p4 = crv.point2.add(crv.getNormalAtTime(1).multiply(dist));
        
        var pointDist = crv.point1.getDistance(crv.point2);
        
        var f = p1.getDistance(p4) / pointDist;
        if (crv.point2.subtract(crv.point1).dot(p4.subtract(p1)) < 0) {
            f = -f; 
        }
        
        return new paper.Curve(p1, crv.handle1.multiply(f), crv.handle2.multiply(f), p4);
    },

    getOffsetCurves: function (curve, offset, method) {
        var errorThreshold = 0.01,
            radius = Math.abs(offset),
            offsetMethod = this['offsetCurve_' + (method || 'middle')],
            that = this;

        function offsetCurce(curve, curves, recursion) {
            var offsetCurve = offsetMethod.call(that, curve, offset),
                cv = curve.getValues(),
                ov = offsetCurve.getValues(),
                count = 16,
                error = 0;
            for (var i = 1; i < count; i++) {
                var t = i / count,
                    p = paper.Curve.getPoint(cv, t),
                    n = paper.Curve.getNormal(cv, t),
                    roots = paper.Curve.getCurveLineIntersections(ov, p.x, p.y, n.x, n.y),
                    dist = 2 * radius;
                for (var j = 0, l = roots.length; j < l; j++) {
                    var d = paper.Curve.getPoint(ov, roots[j]).getDistance(p);
                    if (d < dist)
                        dist = d;
                }
                var err = Math.abs(radius - dist);
                if (err > error)
                    error = err;
            }
            if (error > errorThreshold && recursion++ < 8) {
                if (error === radius) {
                    
                }
                var curve2 = curve.divideAtTime(that.getAverageTangentTime(cv));
                offsetCurce(curve, curves, recursion);
                offsetCurce(curve2, curves, recursion);
            } else {
                curves.push(offsetCurve);
            }
            return curves;
        }

        return offsetCurce(curve, [], 0);
    },

    splitCurveForOffseting: function (curve) {
        var curves = [curve.clone()], 
            that = this;
        if (curve.isStraight())
            return curves;

        function splitAtRoots(index, roots) {
            for (var i = 0, prevT, l = roots && roots.length; i < l; i++) {
                var t = roots[i],
                    curve = curves[index].divideAtTime(
                        
                        i ? (t - prevT) / (1 - prevT) : t);
                prevT = t;
                if (curve)
                    curves.splice(++index, 0, curve);
            }
        }

        
        
        function splitLargeAngles(index, recursion) {
            var curve = curves[index],
                v = curve.getValues(),
                n1 = Curve.getNormal(v, 0),
                n2 = Curve.getNormal(v, 1).negate(),
                cos = n1.dot(n2);
            if (cos > -0.5 && ++recursion < 4) {
                curves.splice(index + 1, 0,
                    curve.divideAtTime(that.getAverageTangentTime(v)));
                splitLargeAngles(index + 1, recursion);
                splitLargeAngles(index, recursion);
            }
        }

        
        var info = curve.classify();
        if (info.roots && info.type !== 'loop') {
            splitAtRoots(0, info.roots);
        }

        
        for (var i = curves.length - 1; i >= 0; i--) {
            splitAtRoots(i, paper.Curve.getPeaks(curves[i].getValues()));
        }

        
        for (var i = curves.length - 1; i >= 0; i--) {
            
        }
        return curves;
    },

    getAverageTangentTime: function (v) {
        var tan = paper.Curve.getTangent(v, 0).add(paper.Curve.getTangent(v, 1)),
            tx = tan.x,
            ty = tan.y,
            abs = Math.abs,
            flip = abs(ty) < abs(tx),
            s = flip ? ty / tx : tx / ty,
            ia = flip ? 1 : 0, 
            io = ia ^ 1,       
            a0 = v[ia + 0], o0 = v[io + 0],
            a1 = v[ia + 2], o1 = v[io + 2],
            a2 = v[ia + 4], o2 = v[io + 4],
            a3 = v[ia + 6], o3 = v[io + 6],
            aA = -a0 + 3 * a1 - 3 * a2 + a3,
            aB = 3 * a0 - 6 * a1 + 3 * a2,
            aC = -3 * a0 + 3 * a1,
            oA = -o0 + 3 * o1 - 3 * o2 + o3,
            oB = 3 * o0 - 6 * o1 + 3 * o2,
            oC = -3 * o0 + 3 * o1,
            roots = [],
            epsilon = paper.Numerical.CURVETIME_EPSILON,
            count = paper.Numerical.solveQuadratic(
                3 * (aA - s * oA),
                2 * (aB - s * oB),
                aC - s * oC, roots,
                epsilon, 1 - epsilon);
        
        return count > 0 ? roots[0] : 0.5;
    },

    addRoundJoin: function (path, dest, center, radius) {
        
        var middle = path.lastSegment.point.add(dest).divide(2),
            through = center.add(middle.subtract(center).normalize(radius));
        path.arcTo(through, dest);
    },
};

const canvas = document.getElementById('canvas');

var color = 'rgba(0, 0, 0)';

paper.setup(canvas);
$(document).ready(function () {
    $('#toolSelect').trigger('change');
    $('#colorSelect').trigger('change');
});

$('#toolSelect').on('change', function () {
    var selectedTool = $('#toolSelect').find(":selected").val();

    if (selectedTool === 'draw') {
        drawTool.activate();
    }
    if (selectedTool === 'erase') {
        eraseTool.activate();
    }
});

var topLayer = new paper.Layer();

$('#colorSelect').on('change', function () {
    var selectedColor = document.getElementById('colorSelect').value;
    color = selectedColor;
    topLayer.strokeColor = selectedColor;
});


var drawPath;
var drawTool = new paper.Tool();
var somethingIsDrawn = false;

drawTool.onMouseDown = function (event) {
    somethingIsDrawn = true;
    drawPath = new paper.Path({
        strokeWidth: 5,
        strokeCap: 'round',
        strokeJoin: 'round'
    });
    drawPath.strokeColor = color;
    drawPath.add(event.point);
}

drawTool.onMouseDrag = function (event) {
    drawPath.add(event.point);
}

drawTool.onMouseUp = function (event) {

}

var eraseTool = new paper.Tool();

var erasePath, tmpGroup, mask


eraseTool.onMouseDown = function (event) {

    erasePath = new paper.Path({
        strokeWidth: 50,
        strokeCap: 'round',
        strokeJoin: 'round',
        strokeColor: 'white'
    });

    tmpGroup = new paper.Group({
        children: topLayer.removeChildren(),
        blendMode: 'source-out',
        insert: false
    });

    mask = new paper.Group({
        children: [erasePath, tmpGroup],
        blendMode: 'source-over'
    });
}

eraseTool.onMouseDrag = function (event) {
    erasePath.add(event.point)
}

eraseTool.onMouseUp = function (event) {

    var eraseRadius = (50) / 2

    var outerPath = OffsetUtils.offsetPath(erasePath, eraseRadius)
    var innerPath = OffsetUtils.offsetPath(erasePath, -eraseRadius)
    erasePath.remove()

    outerPath.insert = false
    innerPath.insert = false
    innerPath.reverse()

    var deleteShape = new paper.Path({
        closed: true,
        insert: false
    })
    deleteShape.addSegments(outerPath.segments)
    deleteShape.addSegments(innerPath.segments)

    var endCaps = new paper.CompoundPath({
        children: [
            new paper.Path.Circle({
                center: erasePath.firstSegment.point,
                radius: eraseRadius
            }),
            new paper.Path.Circle({
                center: erasePath.lastSegment.point,
                radius: eraseRadius
            })
        ],
        insert: false
    })

    deleteShape = deleteShape.unite(endCaps)
    deleteShape.simplify()

    var items = tmpGroup.getItems({ overlapping: deleteShape.bounds })

    items.forEach(function (item) {
        var result = item.subtract(deleteShape, {
            trace: false,
            insert: false
        })

        if (result.children) {
            item.parent.insertChildren(item.index, result.removeChildren())
            item.remove()
        } else {
            if (result.length === 0) {
                item.remove()
            } else {
                item.replaceWith(result)
            }
        }
    })

    topLayer.addChildren(tmpGroup.removeChildren())
    mask.remove()
}
$('#clear-layer').on('click', function() {
    topLayer.removeChildren()
});

$("#FormControlTypeSelect").on('change', (e) => {
    if ($('#FormControlTypeSelect').find(":selected").text() == "P") {
        document.getElementById('CutsContours').disabled = false;
        document.getElementById('CutsContours').checked = true;
    }
    else {
        document.getElementById('CutsContours').disabled = true;
        document.getElementById('CutsContours').checked = false;
    }
})

$('#sendData').on('click', async function() {

    if ($('#symbolName').val() === '' || !somethingIsDrawn || $("#FormControlCategorySelect")[0].selectedIndex === 0 || $("#FormControlTypeSelect")[0].selectedIndex === 0) {
        $('#errorDraw').css("display", "block");
    }
    else {
        var formData = new FormData();
    
        var dataBlob = new Blob([paper.project.exportSVG({asString: true})], {type: 'image/svg+xml;charset=utf-8'});
    
        formData.append("DTA_USR",  $('#emailAddress').val());
        formData.append("DTA_TXT",  $('#symbolName').val());
        formData.append("UID_CAT",  $('#FormControlCategorySelect').find(":selected").val());
        formData.append("UID_TYP",  $('#FormControlTypeSelect').find(":selected").val());
        
        formData.append("DTA_R", parseInt(document.getElementById('colorSelect').value.substr(1,2), 16));
        formData.append("DTA_G", parseInt(document.getElementById('colorSelect').value.substr(3,2), 16));
        formData.append("DTA_B", parseInt(document.getElementById('colorSelect').value.substr(5,2), 16));

        formData.append("DTA_DESC", $('#FormControlTextarea').val());
        formData.append("DTA_ATT",  $('#File').prop('files')[0]);
        formData.append("DTA_SVG",  dataBlob, 'custom.svg');
        formData.append("DTA_CUTS_CONTOURS", document.getElementById('CutsContours').checked);
        
        await $.ajax({
            type: "POST",
            url: "/symbols",
            data: formData,
            processData: false,
            contentType: false,
            success: function(res) {
                $("#successDraw").show();
                $("#emailAddress").val('');
                $("#symbolName").val('');
                $('#FormControlCategorySelect').prop('selectedIndex',0);
                $('#FormControlTypeSelect').prop('selectedIndex',0);
                $("#FormControlTextarea").val('');
                $("#File").val('');
                $("#FileLabel").html(`<i class="fas fa-upload"></i> Dodajte datoteko`);
                
                document.getElementById('CutsContours').checked = false;
                document.getElementById('CutsContours').disabled = true;

                topLayer.removeChildren();
            },
            error: function(err) {
                console.log(err);
            }
        })
    }

});
