const swaggerJsdoc = require("swagger-jsdoc");

const options = {
    definition: {
      openapi: "3.0.0",
      info: {
        title: "E-Topografski-Kljuc",
        version: "0.1.0",
        description:
          "CRUD API for suggesting and approving symbols.",
      },
      servers: [
        {
          url: "http://localhost:3001/",
        },
      ],
    },
    apis: ["./routes/symbols.js", "./routes/users.js", "./routes/categories.js", "./routes/colors.js", "./routes/types.js", "./routes/pdf.js"],
    components: {
      securitySchemes: {
        cookieAuth: {
          type: 'apiKey',
          in: 'cookie',
          name: 'connect.sid'
        }
      }
    }
  };
  
  const specs = swaggerJsdoc(options);

  module.exports = specs